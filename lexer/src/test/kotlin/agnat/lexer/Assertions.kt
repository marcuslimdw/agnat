package agnat.lexer

import assertk.Assert
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Assertions

internal fun Assert<LexResult>.isMatchOf(expected: Match) = given { actual ->
    when (actual) {
        is Match -> isEqualTo(expected)
        else -> Assertions.fail("Expected $actual to be Match but was ${actual::class}")
    }
}

internal fun Assert<LexResult>.isLexerError(): Assert<LexFailure> = transform { actual ->
    when (actual) {
        is LexFailure -> actual
        else -> Assertions.fail("Expected $actual to be LexerError but was ${actual::class}")
    }
}

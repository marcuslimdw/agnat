package agnat.lexer

import agnat.lexer.TokenType.*
import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test
import java.io.FileNotFoundException

internal class LexerTest {

    @Test
    fun `should correctly lex an empty program`() {
        val actual = Lexer.lex("")
        val expected = listOf(Token(EOF, "", Position(0, 0)))
        assertThat(actual).isEqualTo(expected)
    }

    @Suppress("LongMethod")
    @Test
    fun `should correctly lex a program`() {
        val actual = Lexer.lex(source)
        val expected = listOf(
            Token(DEF, "def", Position(3, 0)),
            Token(IDENTIFIER, "max_plus_two", Position(3, 4)),
            Token(OPEN_PARENTHESIS, "(", Position(3, 16)),
            Token(IDENTIFIER, "left", Position(3, 17)),
            Token(COLON, ":", Position(3, 21)),
            Token(IDENTIFIER, "Int", Position(3, 23)),
            Token(COMMA, ",", Position(3, 26)),
            Token(IDENTIFIER, "right", Position(3, 28)),
            Token(COLON, ":", Position(3, 33)),
            Token(IDENTIFIER, "Int", Position(3, 35)),
            Token(CLOSE_PARENTHESIS, ")", Position(3, 38)),
            Token(EQUAL, "=", Position(3, 40)),
            Token(OPEN_BRACE, "{", Position(3, 42)),
            Token(NEWLINE, "\n", Position(3, 43)),
            Token(NEWLINE, "\n", Position(5, 0)),
            Token(VAL, "val", Position(6, 4)),
            Token(IDENTIFIER, "max", Position(6, 8)),
            Token(EQUAL, "=", Position(6, 12)),
            Token(IF, "if", Position(6, 14)),
            Token(IDENTIFIER, "left", Position(6, 17)),
            Token(GREATER_EQUAL, ">=", Position(6, 22)),
            Token(IDENTIFIER, "right", Position(6, 25)),
            Token(IDENTIFIER, "left", Position(6, 31)),
            Token(ELSE, "else", Position(6, 36)),
            Token(IDENTIFIER, "right", Position(6, 41)),
            Token(NEWLINE, "\n", Position(6, 46)),
            Token(RETURN, "return", Position(7, 4)),
            Token(IDENTIFIER, "max", Position(7, 11)),
            Token(PLUS, "+", Position(7, 15)),
            Token(INT_LITERAL, "2", Position(7, 17)),
            Token(NEWLINE, "\n", Position(7, 18)),
            Token(CLOSE_BRACE, "}", Position(8, 0)),
            Token(NEWLINE, "\n", Position(8, 1)),
            Token(NEWLINE, "\n", Position(9, 0)),
            Token(ENUM, "enum", Position(10, 0)),
            Token(IDENTIFIER, "OptionInt", Position(10, 5)),
            Token(OPEN_PARENTHESIS, "(", Position(10, 14)),
            Token(NEWLINE, "\n", Position(10, 15)),
            Token(IDENTIFIER, "Some", Position(11, 4)),
            Token(OPEN_PARENTHESIS, "(", Position(11, 8)),
            Token(IDENTIFIER, "value", Position(11, 9)),
            Token(COLON, ":", Position(11, 14)),
            Token(IDENTIFIER, "Int", Position(11, 16)),
            Token(CLOSE_PARENTHESIS, ")", Position(11, 19)),
            Token(COMMA, ",", Position(11, 20)),
            Token(NEWLINE, "\n", Position(11, 21)),
            Token(IDENTIFIER, "None", Position(12, 4)),
            Token(NEWLINE, "\n", Position(12, 8)),
            Token(CLOSE_PARENTHESIS, ")", Position(13, 0)),
            Token(NEWLINE, "\n", Position(13, 1)),
            Token(NEWLINE, "\n", Position(14, 0)),
            Token(STRUCT, "struct", Position(15, 0)),
            Token(IDENTIFIER, "SomeData", Position(15, 7)),
            Token(OPEN_PARENTHESIS, "(", Position(15, 15)),
            Token(NEWLINE, "\n", Position(15, 16)),
            Token(IDENTIFIER, "option_int", Position(16, 4)),
            Token(COLON, ":", Position(16, 14)),
            Token(IDENTIFIER, "OptionInt", Position(16, 16)),
            Token(COMMA, ",", Position(16, 25)),
            Token(NEWLINE, "\n", Position(16, 26)),
            Token(IDENTIFIER, "string", Position(17, 4)),
            Token(COLON, ":", Position(17, 10)),
            Token(IDENTIFIER, "String", Position(17, 12)),
            Token(NEWLINE, "\n", Position(17, 18)),
            Token(CLOSE_PARENTHESIS, ")", Position(18, 0)),
            Token(NEWLINE, "\n", Position(18, 1)),
            Token(NEWLINE, "\n", Position(19, 0)),
            Token(ABSTRACT, "abstract", Position(20, 0)),
            Token(IDENTIFIER, "Show", Position(20, 9)),
            Token(OPEN_BRACE, "{", Position(20, 14)),
            Token(NEWLINE, "\n", Position(20, 15)),
            Token(NEWLINE, "\n", Position(21, 0)),
            Token(DEF, "def", Position(22, 4)),
            Token(IDENTIFIER, "show", Position(22, 8)),
            Token(OPEN_PARENTHESIS, "(", Position(22, 12)),
            Token(IDENTIFIER, "self", Position(22, 13)),
            Token(CLOSE_PARENTHESIS, ")", Position(22, 17)),
            Token(COLON, ":", Position(22, 18)),
            Token(IDENTIFIER, "String", Position(22, 20)),
            Token(NEWLINE, "\n", Position(22, 26)),
            Token(CLOSE_BRACE, "}", Position(23, 0)),
            Token(NEWLINE, "\n", Position(23, 1)),
            Token(NEWLINE, "\n", Position(24, 0)),
            Token(CONCRETE, "concrete", Position(25, 0)),
            Token(IDENTIFIER, "Printable", Position(25, 9)),
            Token(FOR, "for", Position(25, 19)),
            Token(IDENTIFIER, "SomeData", Position(25, 23)),
            Token(OPEN_BRACE, "{", Position(25, 32)),
            Token(NEWLINE, "\n", Position(25, 33)),
            Token(NEWLINE, "\n", Position(26, 0)),
            Token(DEF, "def", Position(27, 4)),
            Token(IDENTIFIER, "show", Position(27, 8)),
            Token(OPEN_PARENTHESIS, "(", Position(27, 12)),
            Token(IDENTIFIER, "self", Position(27, 13)),
            Token(CLOSE_PARENTHESIS, ")", Position(27, 17)),
            Token(COLON, ":", Position(27, 18)),
            Token(IDENTIFIER, "String", Position(27, 20)),
            Token(EQUAL, "=", Position(27, 27)),
            Token(IDENTIFIER, "self", Position(27, 29)),
            Token(PERIOD, ".", Position(27, 33)),
            Token(IDENTIFIER, "string", Position(27, 34)),
            Token(NEWLINE, "\n", Position(27, 40)),
            Token(CLOSE_BRACE, "}", Position(28, 0)),
            Token(NEWLINE, "\n", Position(28, 1)),
            Token(EOF, "", Position(28, 1))
        )
        assertThat(actual).isEqualTo(expected)
    }

    companion object {

        private const val sourceFilename = "/source.agn"
        private val source = this::class.java.getResource(sourceFilename)?.readText()
            ?: throw FileNotFoundException("Couldn't find source file at '${sourceFilename}' for acceptance test.")
    }
}

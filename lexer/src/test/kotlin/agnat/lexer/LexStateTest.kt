package agnat.lexer

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class LexStateTest {

    @Nested
    inner class `plus should` {

        @Test
        fun `combine a state with a horizontal offset correctly`() {
            val state = LexState("starting\nstate", 3, Position(0, 3))
            val offset = HorizontalOffset(3)
            val actual = state + offset
            val expected = LexState("starting\nstate", 6, Position(0, 6))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `combine a state with a vertical offset correctly`() {
            val state = LexState("starting\nstate", 3, Position(0, 3))
            val offset = VerticalOffset(3, 1)
            val actual = state + offset
            val expected = LexState("starting\nstate", 6, Position(1, 0))
            assertThat(actual).isEqualTo(expected)
        }
    }
}

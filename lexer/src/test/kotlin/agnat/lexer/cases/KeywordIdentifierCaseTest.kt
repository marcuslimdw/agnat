package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexState
import agnat.lexer.Match
import agnat.lexer.TokenType.IDENTIFIER
import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class KeywordIdentifierCaseTest {

    @Nested
    inner class `should extract` {

        @Test
        fun `an identifier containing only letters`() {
            val lexeme = "abc"
            val state = LexState.start(lexeme)
            val actual = KeywordIdentifierCase.extract(state)
            val expected = Match(IDENTIFIER, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `an identifier starting with an underscore`() {
            val lexeme = "_abc"
            val state = LexState.start(lexeme)
            val actual = KeywordIdentifierCase.extract(state)
            val expected = Match(IDENTIFIER, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `an identifier containing letters and numbers`() {
            val lexeme = "abc123"
            val state = LexState.start(lexeme)
            val actual = KeywordIdentifierCase.extract(state)
            val expected = Match(IDENTIFIER, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `an identifier containing letters and underscores`() {
            val lexeme = "abc_def_ghi"
            val state = LexState.start(lexeme)
            val actual = KeywordIdentifierCase.extract(state)
            val expected = Match(IDENTIFIER, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `an identifier containing letters, numbers, and underscores`() {
            val lexeme = "abc_123_ghi"
            val state = LexState.start(lexeme)
            val actual = KeywordIdentifierCase.extract(state)
            val expected = Match(IDENTIFIER, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }
    }
}

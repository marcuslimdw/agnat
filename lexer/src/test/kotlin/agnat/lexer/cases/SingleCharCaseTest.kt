package agnat.lexer.cases

import agnat.lexer.*
import agnat.lexer.TokenType.*
import assertk.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class SingleCharCaseTest {

    @Nested
    inner class `should extract` {

        @Test
        fun `a newline`() {
            val state = LexState.start("\n")
            val actual = SingleCharCase.extract(state)
            val expected = Match(NEWLINE, "\n", VerticalOffset(1, 1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `an open parenthesis`() {
            val state = LexState.start("(")
            val actual = SingleCharCase.extract(state)
            val expected = Match(OPEN_PARENTHESIS, "(", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a close parenthesis`() {
            val state = LexState.start(")")
            val actual = SingleCharCase.extract(state)
            val expected = Match(CLOSE_PARENTHESIS, ")", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `an open bracket`() {
            val state = LexState.start("[")
            val actual = SingleCharCase.extract(state)
            val expected = Match(OPEN_BRACKET, "[", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a close bracket`() {
            val state = LexState.start("]")
            val actual = SingleCharCase.extract(state)
            val expected = Match(CLOSE_BRACKET, "]", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `an open brace`() {
            val state = LexState.start("{")
            val actual = SingleCharCase.extract(state)
            val expected = Match(OPEN_BRACE, "{", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a close brace`() {
            val state = LexState.start("}")
            val actual = SingleCharCase.extract(state)
            val expected = Match(CLOSE_BRACE, "}", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a comma`() {
            val state = LexState.start(",")
            val actual = SingleCharCase.extract(state)
            val expected = Match(COMMA, ",", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a period`() {
            val state = LexState.start(".")
            val actual = SingleCharCase.extract(state)
            val expected = Match(PERIOD, ".", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a plus`() {
            val state = LexState.start("+")
            val actual = SingleCharCase.extract(state)
            val expected = Match(PLUS, "+", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a minus`() {
            val state = LexState.start("-")
            val actual = SingleCharCase.extract(state)
            val expected = Match(MINUS, "-", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a asterisk`() {
            val state = LexState.start("*")
            val actual = SingleCharCase.extract(state)
            val expected = Match(ASTERISK, "*", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a forward slash`() {
            val state = LexState.start("/")
            val actual = SingleCharCase.extract(state)
            val expected = Match(FORWARD_SLASH, "/", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `an ampersand`() {
            val state = LexState.start("&")
            val actual = SingleCharCase.extract(state)
            val expected = Match(AMPERSAND, "&", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a bar`() {
            val state = LexState.start("|")
            val actual = SingleCharCase.extract(state)
            val expected = Match(BAR, "|", HorizontalOffset(1))
            assertThat(actual).isMatchOf(expected)
        }
    }
}

package agnat.lexer.cases

import agnat.lexer.*
import agnat.lexer.TokenType.STRING_LITERAL
import agnat.lexer.failure.UnterminatedString
import assertk.assertThat
import assertk.assertions.hasClass
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class StringLiteralCaseTest {

    @Nested
    inner class `should extract` {

        @Test
        fun `an empty string literal`() {
            val lexeme = "\"\""
            val state = LexState.start(lexeme)
            val actual = StringLiteralCase.extract(state)
            val expected = Match(STRING_LITERAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `a nonempty string literal containing escaped quotes non-greedily`() {
            val lexeme = "\"a string\\\"with more stuff\" \"without this\""
            val state = LexState.start(lexeme)
            val actual = StringLiteralCase.extract(state)
            val expectedLexeme = "\"a string\"with more stuff\""
            val expected = Match(STRING_LITERAL, expectedLexeme, HorizontalOffset(27))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `a nonempty string literal not containing newlines`() {
            val lexeme = "\"a string\""
            val state = LexState.start(lexeme)
            val actual = StringLiteralCase.extract(state)
            val expected = Match(STRING_LITERAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `a nonempty string literal containing escaped newlines`() {
            val lexeme = "\"\\nfirst line\\nsecond line\""
            val state = LexState.start(lexeme)
            val actual = StringLiteralCase.extract(state)
            val expectedLexeme = "\"\nfirst line\nsecond line\""
            val expected = Match(STRING_LITERAL, expectedLexeme, HorizontalOffset(27))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `a nonempty string literal containing unescaped newlines`() {
            val lexeme = "\"\nfirst line\nsecond line\n\""
            val state = LexState.start(lexeme)
            val actual = StringLiteralCase.extract(state)
            val expected = Match(STRING_LITERAL, lexeme, VerticalOffset(26, 3))
            assertThat(actual).isEqualTo(expected)
        }
    }

    @Nested
    inner class `should fail to extract` {

        @Test
        fun `an unterminated string literal`() {
            val state = LexState.start("\"an unterminated string")
            val actual = StringLiteralCase.extract(state)
            assertThat(actual).isLexerError().hasClass(UnterminatedString::class)
        }
    }

    @Nested
    inner class `should resolve an escaped` {

        @Test
        fun tab() {
            val actual = StringLiteralCase.resolveEscapes("\\t")
            assertThat(actual).isEqualTo("\t")
        }

        @Test
        fun newline() {
            val actual = StringLiteralCase.resolveEscapes("\\n")
            assertThat(actual).isEqualTo("\n")
        }

        @Test
        fun `carriage return`() {
            val actual = StringLiteralCase.resolveEscapes("\\r")
            assertThat(actual).isEqualTo("\r")
        }

        @Test
        fun `double quote`() {
            val actual = StringLiteralCase.resolveEscapes("\\\"")
            assertThat(actual).isEqualTo("\"")
        }
    }
}

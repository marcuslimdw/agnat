package agnat.lexer.cases

import agnat.lexer.LexState
import agnat.lexer.Skip
import agnat.lexer.VerticalOffset
import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

internal class CommentCaseTest {

    @Test
    fun `should extract a comment`() {
        val state = LexState.start("# a comment\n")
        val actual = CommentCase.extract(state)
        val expected = Skip(VerticalOffset(12, 1))
        assertThat(actual).isEqualTo(expected)
    }
}

package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexState
import agnat.lexer.Match
import agnat.lexer.TokenType.*
import agnat.lexer.isMatchOf
import assertk.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class SingleDoubleCharCaseTest {

    @Nested
    inner class `should extract` {

        @Test
        fun `a single equals`() {
            val lexeme = "="
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(EQUAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a double equals`() {
            val lexeme = "=="
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(EQUAL_EQUAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a single bang`() {
            val lexeme = "!"
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(BANG, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a bang followed by an equal`() {
            val lexeme = "!="
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(BANG_EQUAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a single greater than`() {
            val lexeme = ">"
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(GREATER, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a greater than followed by an equals`() {
            val lexeme = ">="
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(GREATER_EQUAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a less than`() {
            val lexeme = "<"
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(LESS, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a less than followed by an equal`() {
            val lexeme = "<="
            val state = LexState.start(lexeme)
            val actual = SingleDoubleCharCase.extract(state)
            val expected = Match(LESS_EQUAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }
    }
}

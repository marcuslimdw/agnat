package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexState
import agnat.lexer.Skip
import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class WhitespaceCaseTest {

    @Nested
    inner class `should extract` {

        @Test
        fun `a space`() {
            val state = LexState.start(" ")
            val actual = WhitespaceCase.extract(state)
            val expected = Skip(HorizontalOffset(1))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `multiple spaces`() {
            val state = LexState.start("    ")
            val actual = WhitespaceCase.extract(state)
            val expected = Skip(HorizontalOffset(4))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `a tab`() {
            val state = LexState.start("\t")
            val actual = WhitespaceCase.extract(state)
            val expected = Skip(HorizontalOffset(1))
            assertThat(actual).isEqualTo(expected)
        }

        @Test
        fun `a carriage return`() {
            val state = LexState.start("\r")
            val actual = WhitespaceCase.extract(state)
            val expected = Skip(HorizontalOffset(1))
            assertThat(actual).isEqualTo(expected)
        }
    }
}

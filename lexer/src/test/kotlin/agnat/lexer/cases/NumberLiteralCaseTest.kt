package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexState
import agnat.lexer.Match
import agnat.lexer.TokenType.FLOAT_LITERAL
import agnat.lexer.TokenType.INT_LITERAL
import agnat.lexer.isMatchOf
import assertk.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class NumberLiteralCaseTest {

    @Nested
    inner class `should extract` {

        @Test
        fun `an int literal`() {
            val lexeme = "1234567890"
            val state = LexState.start(lexeme)
            val actual = NumberLiteralCase.extract(state)
            val expected = Match(INT_LITERAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }

        @Test
        fun `a float literal`() {
            val lexeme = "12345.6789"
            val state = LexState.start(lexeme)
            val actual = NumberLiteralCase.extract(state)
            val expected = Match(FLOAT_LITERAL, lexeme, HorizontalOffset(lexeme.length))
            assertThat(actual).isMatchOf(expected)
        }
    }
}

package agnat.lexer

data class Token(
    val tokenType: TokenType,
    val lexeme: String,
    val position: Position,
)

package agnat.lexer

import agnat.failure.Failure

sealed interface LexResult {

    val offset: Offset
}

internal data class Match(
    val tokenType: TokenType,
    val lexeme: String,
    override val offset: Offset
) : LexResult

internal data class Skip(
    override val offset: Offset
) : LexResult

internal interface LexFailure : Failure, LexResult {

    override val offset: Offset get() = HorizontalOffset(1)
}

package agnat.lexer

internal data class LexState(
    val source: String,
    val rawOffset: Int,
    val position: Position
) {

    val current: Char? = source.getOrNull(rawOffset)

    operator fun plus(offset: Offset): LexState {
        val newPosition = when (offset) {
            is HorizontalOffset -> position.copy(column = position.column + offset.size)
            is VerticalOffset -> Position(position.row + offset.newlineCount, 0)
        }
        return LexState(source, rawOffset + offset.size, newPosition)
    }

    fun match(regex: Regex): MatchResult? = regex.find(source, rawOffset)

    companion object {

        fun start(source: String) = LexState(source, 0, Position.initial)
    }
}

package agnat.lexer

sealed interface Offset {

    val size: Int
}

@JvmInline
internal value class HorizontalOffset(override val size: Int) : Offset

internal data class VerticalOffset(override val size: Int, val newlineCount: Int) : Offset

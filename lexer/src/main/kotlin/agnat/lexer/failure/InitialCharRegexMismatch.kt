package agnat.lexer.failure

import agnat.lexer.LexFailure

data class InitialCharRegexMismatch(val initialChars: Set<Char>, val regex: Regex) : LexFailure {

    override val reason: String = "The regex $regex failed to match a string starting with one of $initialChars"
}

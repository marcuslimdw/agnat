package agnat.lexer.failure

import agnat.lexer.LexFailure
import agnat.lexer.TokenType

data class InitialCharTokenMapMismatch(val char: Char?, val tokenMap: Map<Char, TokenType>) : LexFailure {

    override val reason: String = ""
}

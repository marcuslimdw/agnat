package agnat.lexer.failure

import agnat.lexer.LexFailure
import agnat.lexer.Position

data class UnterminatedString(val position: Position) : LexFailure {

    override val reason: String = "Encountered an unterminated string starting at $position"
}

package agnat.lexer.failure

import agnat.lexer.LexFailure

data class UnrecognisedChar(val char: Char) : LexFailure {

    override val reason: String = "Encountered an unrecognised character $char"
}

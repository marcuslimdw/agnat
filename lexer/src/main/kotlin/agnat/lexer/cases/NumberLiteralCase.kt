package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexResult
import agnat.lexer.Match
import agnat.lexer.TokenType
import agnat.lexer.TokenType.FLOAT_LITERAL
import agnat.lexer.TokenType.INT_LITERAL

internal object NumberLiteralCase : RegexCase() {

    override val initialChars: Set<Char> = ('0'..'9').toSet()

    override val regex = """(\d+)(\.\d+)?""".toRegex()

    override fun process(matchValue: String): LexResult = Match(
        resolveLiteral(matchValue),
        matchValue,
        HorizontalOffset(matchValue.length)
    )

    private fun resolveLiteral(value: String): TokenType = if ('.' in value) FLOAT_LITERAL else INT_LITERAL
}

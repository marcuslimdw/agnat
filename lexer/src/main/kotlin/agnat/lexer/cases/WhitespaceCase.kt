package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexResult
import agnat.lexer.Skip

internal object WhitespaceCase : RegexCase() {

    override val initialChars: Set<Char> = setOf(' ', '\t', '\r')

    override val regex = """[ \t\r]+""".toRegex()

    override fun process(matchValue: String): LexResult = Skip(HorizontalOffset(matchValue.length))
}

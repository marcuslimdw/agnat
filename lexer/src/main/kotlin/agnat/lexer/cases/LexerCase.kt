package agnat.lexer.cases

import agnat.lexer.LexResult
import agnat.lexer.LexState

internal interface LexerCase {

    val initialChars: Set<Char>

    fun extract(state: LexState): LexResult
}

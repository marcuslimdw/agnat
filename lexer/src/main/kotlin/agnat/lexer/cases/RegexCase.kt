package agnat.lexer.cases

import agnat.lexer.LexResult
import agnat.lexer.LexState
import agnat.lexer.failure.InitialCharRegexMismatch

internal abstract class RegexCase : LexerCase {

    protected abstract val regex: Regex

    protected abstract fun process(matchValue: String): LexResult

    protected open fun handleMatchFailure(state: LexState): LexResult = InitialCharRegexMismatch(initialChars, regex)

    final override fun extract(state: LexState): LexResult = state
        .match(regex)
        ?.let { process(it.value) }
        ?: handleMatchFailure(state)
}

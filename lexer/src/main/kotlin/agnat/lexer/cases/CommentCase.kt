package agnat.lexer.cases

import agnat.lexer.LexResult
import agnat.lexer.Skip
import agnat.lexer.VerticalOffset

internal object CommentCase : RegexCase() {

    override val initialChars: Set<Char> = setOf('#')

    override val regex = "#.+\\n".toRegex()

    override fun process(matchValue: String): LexResult = Skip(VerticalOffset(matchValue.length, 1))
}

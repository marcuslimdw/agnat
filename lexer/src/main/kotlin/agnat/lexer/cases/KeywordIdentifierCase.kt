package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexResult
import agnat.lexer.Match
import agnat.lexer.TokenType.*

internal object KeywordIdentifierCase : RegexCase() {

    override val initialChars: Set<Char> = ('A'..'Z').toSet() + ('a'..'z').toSet() + '_'

    private val tokenMap = mapOf(
        "true" to TRUE_LITERAL,
        "false" to FALSE_LITERAL,

        "if" to IF,
        "else" to ELSE,
        "for" to FOR,
        "while" to WHILE,

        "return" to RETURN,

        "def" to DEF,
        "val" to VAL,
        "var" to VAR,

        "struct" to STRUCT,
        "enum" to ENUM,
        "implementation" to IMPLEMENTATION,
        "abstract" to ABSTRACT,
        "concrete" to CONCRETE,
        "newtype" to NEWTYPE,
        "typealias" to TYPEALIAS,
    )

    override val regex = "[A-Za-z_][\\w]*".toRegex()

    override fun process(matchValue: String): LexResult = Match(
        tokenMap[matchValue] ?: IDENTIFIER, matchValue, HorizontalOffset(matchValue.length)
    )
}

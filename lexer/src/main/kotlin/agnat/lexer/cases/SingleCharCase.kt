package agnat.lexer.cases

import agnat.lexer.*
import agnat.lexer.TokenType.*
import agnat.lexer.failure.InitialCharTokenMapMismatch

internal object SingleCharCase : LexerCase {

    private val tokenMap = mapOf(
        '\n' to NEWLINE,
        '(' to OPEN_PARENTHESIS,
        ')' to CLOSE_PARENTHESIS,
        '[' to OPEN_BRACKET,
        ']' to CLOSE_BRACKET,
        '{' to OPEN_BRACE,
        '}' to CLOSE_BRACE,
        ',' to COMMA,
        '.' to PERIOD,
        '+' to PLUS,
        '-' to MINUS,
        '*' to ASTERISK,
        '/' to FORWARD_SLASH,
        '&' to AMPERSAND,
        '|' to BAR,
        ':' to COLON,
    )

    override val initialChars: Set<Char> = tokenMap.keys

    override fun extract(state: LexState): LexResult = state.current.let { char ->
        tokenMap[char]
            ?.let { Match(it, char.toString(), if (it == NEWLINE) VerticalOffset(1, 1) else HorizontalOffset(1)) }
            ?: InitialCharTokenMapMismatch(char, tokenMap)
    }
}

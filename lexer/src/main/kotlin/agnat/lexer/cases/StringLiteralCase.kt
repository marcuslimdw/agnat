package agnat.lexer.cases

import agnat.lexer.*
import agnat.lexer.TokenType.STRING_LITERAL
import agnat.lexer.failure.UnterminatedString
import kotlin.text.RegexOption.DOT_MATCHES_ALL

internal object StringLiteralCase : RegexCase() {

    private val escapeMap = mapOf(
        "\\t" to "\t",
        "\\n" to "\n",
        "\\r" to "\r",
        "\\\"" to "\"",
    )

    override val initialChars: Set<Char> = setOf('"')

    override val regex = "\".*?(?<!\\\\)\"".toRegex(DOT_MATCHES_ALL)

    override fun process(matchValue: String): LexResult {
        val offset = when (val newlineCount = matchValue.count { char -> char == '\n' }) {
            0 -> HorizontalOffset(matchValue.length)
            else -> VerticalOffset(matchValue.length, newlineCount)
        }
        return Match(STRING_LITERAL, resolveEscapes(matchValue), offset)
    }

    override fun handleMatchFailure(state: LexState): LexResult = UnterminatedString(state.position)

    internal fun resolveEscapes(string: String): String = string.replace("\\\\.".toRegex()) {
        escapeMap.getOrDefault(it.value, it.value)
    }
}

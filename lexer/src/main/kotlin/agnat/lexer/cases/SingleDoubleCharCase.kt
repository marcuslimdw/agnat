package agnat.lexer.cases

import agnat.lexer.HorizontalOffset
import agnat.lexer.LexResult
import agnat.lexer.Match
import agnat.lexer.TokenType.*

internal object SingleDoubleCharCase : RegexCase() {

    private val tokenMap = mapOf(
        "=" to EQUAL,
        "==" to EQUAL_EQUAL,
        "!" to BANG,
        "!=" to BANG_EQUAL,
        ">" to GREATER,
        ">=" to GREATER_EQUAL,
        "<" to LESS,
        "<=" to LESS_EQUAL
    )

    override val initialChars: Set<Char> = tokenMap.keys.map { it.first() }.toSet()

    override val regex = "[=!><]=?".toRegex()

    override fun process(matchValue: String): LexResult = Match(
        tokenMap.getValue(matchValue), matchValue, HorizontalOffset(matchValue.length)
    )
}

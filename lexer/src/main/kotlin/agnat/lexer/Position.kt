package agnat.lexer

data class Position(
    val row: Int,
    val column: Int
) {

    companion object {

        val initial = Position(0, 0)
    }
}

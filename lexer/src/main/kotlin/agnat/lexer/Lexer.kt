package agnat.lexer

import agnat.lexer.TokenType.EOF
import agnat.lexer.TokenType.NEWLINE
import agnat.lexer.cases.*
import agnat.lexer.failure.UnrecognisedChar
import agnat.util.Cons
import agnat.util.LinkedList
import agnat.util.Nil

object Lexer {

    private val lexerCases = LinkedList.from(
        CommentCase,
        WhitespaceCase,
        SingleCharCase,
        SingleDoubleCharCase,
        StringLiteralCase,
        NumberLiteralCase,
        KeywordIdentifierCase
    )

    fun lex(source: String): List<Token> {
        val (tokens, _) = iterate(Nil, Nil, LexState.start(source))
        return when (tokens) {
            is Cons -> Cons(Token(EOF, "", tokens.head.position), tokens)
                .reversed()
                .dropWhile { it.tokenType == NEWLINE }
            is Nil -> listOf(Token(EOF, "", Position.initial))
        }
    }

    private tailrec fun iterate(
        tokens: LinkedList<Token>,
        failures: LinkedList<LexFailure>,
        state: LexState
    ): Pair<LinkedList<Token>, LinkedList<LexFailure>> = when (state.current != null) {
        true -> when (val result = lexNext(lexerCases, state)) {
            is Match -> iterate(
                Cons(Token(result.tokenType, result.lexeme, state.position), tokens),
                failures,
                state + result.offset
            )
            is LexFailure -> iterate(tokens, Cons(result, failures), state + result.offset)
            is Skip -> iterate(tokens, failures, state + result.offset)
        }
        false -> Pair(tokens, failures)
    }

    private tailrec fun lexNext(remainingCases: LinkedList<LexerCase>, state: LexState): LexResult =
        when (remainingCases) {
            is Cons<LexerCase> -> {
                val (head, tail) = remainingCases
                if (state.current in head.initialChars) head.extract(state) else lexNext(tail, state)
            }
            is Nil -> UnrecognisedChar(state.current ?: ' ')
        }
}

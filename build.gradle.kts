import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.30"
    id("io.gitlab.arturbosch.detekt").version("1.18.1")
    id("jacoco")
}

group = "me.marcus"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.5.30")

    testImplementation("org.jetbrains.kotlin:kotlin-test:1.5.30")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.25")
}

detekt {
    autoCorrect = true
}

tasks.check {
    dependsOn("detekt")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

tasks.withType<Detekt>().configureEach {
    jvmTarget = "11"
}

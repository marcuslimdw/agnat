rootProject.name = "agnat"
include("parser")
include("lexer")
include("common")
include("typechecker")
include("resolver")

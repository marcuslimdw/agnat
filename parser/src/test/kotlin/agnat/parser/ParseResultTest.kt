package agnat.parser

import agnat.parser.Common.colon
import agnat.parser.failure.ExpectedExpression
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class ParseResultTest {

    @Nested
    inner class MapTest {

        @Test
        fun `should apply a function to the parsed value in a Success, leaving the new parse state unchanged`() {
            val parseState = ParseState.of(colon)
            val success = Success(1, parseState)
            val result = success.map { it + 1 }
            val expected = Success(2, parseState)
            assertThat(result).isEqualTo(expected)
        }

        @Test
        fun `should leave a ParseFailure unchanged`() {
            val failure = ExpectedExpression(ParseState.of())
            val result = failure.map { throw IllegalStateException() }
            assertThat(result).isEqualTo(result)
        }
    }

    @Nested
    inner class InvertTest {

        @Test
        fun `should leave a Success wrapping a non-null value unchanged`() {
            val parseState = ParseState.of(colon)
            val success = Success(1, parseState)
            val result = success.invert()
            assertThat(result).isNotNull().isEqualTo(result)
        }

        @Test
        fun `should convert a Success wrapping a null value to null`() {
            val parseState = ParseState.of(colon)
            val success = Success(null, parseState)
            val result = success.invert()
            assertThat(result).isNull()
        }

        @Test
        fun `should leave a ParseFailure unchanged`() {
            val failure = ExpectedExpression(ParseState.of())
            val result = failure.invert()
            assertThat(result).isEqualTo(result)
        }
    }

    @Nested
    inner class RecoverWithTest {

        @Test
        fun `should leave a Success unchanged`() {
            val parseState = ParseState.of(colon)
            val success = Success(1, parseState)
            val result = success.recoverWith { Success("a", parseState) }
            assertThat(result).isEqualTo(success)
        }

        @Test
        fun `should replace a ParseFailure with the result of a function`() {
            val parseState = ParseState.of(colon)
            val failure = ExpectedExpression(parseState)
            val result = failure.recoverWith { Success("a", parseState) }
            val expected = Success("a", parseState)
            assertThat(result).isEqualTo(expected)
        }
    }
}

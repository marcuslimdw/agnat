package agnat.parser.combinators

import agnat.ast.node.NoOp
import agnat.lexer.TokenType.INT_LITERAL
import agnat.parser.*
import agnat.parser.Common.comma
import agnat.parser.Common.oneToken
import agnat.parser.failure.UnexpectedToken
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ConsumeTest {

    private val parser = Consume(INT_LITERAL)

    @Test
    fun `should return a NoOp if the input matches the given token type`() {
        val parseState = ParseState.of(oneToken)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(NoOp)
    }

    @Test
    fun `should advance if the input matches the given token type`() {
        val parseState = ParseState.of(oneToken)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().advancingFrom(parseState)
    }

    @Test
    fun `should return UnexpectedToken if the input does not match the given token type`() {
        val parseState = ParseState.of(comma)
        val actual = parser.parse(parseState)
        assertThat(actual).isFailureOf(UnexpectedToken(INT_LITERAL, parseState))
    }

    @Test
    fun `should not advance if the input does not match the given token type`() {
        val parseState = ParseState.of(comma)
        val actual = parser.parse(parseState)
        assertThat(actual).isFailure().withoutAdvancingFrom(parseState)
    }
}

package agnat.parser.combinators

import agnat.lexer.TokenType.COMMA
import agnat.lexer.TokenType.PERIOD
import agnat.parser.*
import agnat.parser.Common.colon
import agnat.parser.Common.comma
import agnat.parser.Common.period
import agnat.parser.failure.UnexpectedToken
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class AnyOfTest {

    private val parser = AnyOf(Capture(PERIOD), Capture(COMMA))

    @Test
    fun `should return the result from the first parser which the input matches`() {
        val parseState = ParseState.of(comma)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(comma)
    }

    @Test
    fun `should advance if the input matches one of the parsers`() {
        val parseState = ParseState.of(comma, period)
        val actual = parser.parse(parseState)
        assertThat(actual).isSuccess().advancingFrom(parseState)
    }

    @Test
    fun `should return the failure of the last parser if the input matches none of the parsers`() {
        val parseState = ParseState.of(colon)
        val actual = parser.parse(parseState)
        assertThat(actual).isFailureOf(UnexpectedToken(COMMA, parseState))
    }
}

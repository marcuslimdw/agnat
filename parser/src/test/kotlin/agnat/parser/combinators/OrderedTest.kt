package agnat.parser.combinators

import agnat.lexer.TokenType.COMMA
import agnat.lexer.TokenType.PERIOD
import agnat.parser.Common.comma
import agnat.parser.Common.period
import agnat.parser.ParseState
import agnat.parser.failure.UnexpectedToken
import agnat.parser.hasParsedFully
import agnat.parser.isFailureOf
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class OrderedTest {

    private val parser = Ordered(Capture(PERIOD), Capture(COMMA), Capture(COMMA))

    @Test
    fun `should return an ordered list of parse results if the input matches all the parsers`() {
        val parseState = ParseState.of(period, comma, comma)
        val result = parser.parse(parseState)
        val expected = listOf(period, comma, comma)
        assertThat(result).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should return the failure of the first parser which the input does not match`() {
        val parseState = ParseState.of(period)
        val actual = parser.parse(parseState)
        val expected = UnexpectedToken(COMMA, parseState.advance())
        assertThat(actual).isFailureOf(expected)
    }
}

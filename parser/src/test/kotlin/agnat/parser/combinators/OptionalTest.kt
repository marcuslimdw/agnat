package agnat.parser.combinators

import agnat.ast.node.NoOp
import agnat.lexer.TokenType.COMMA
import agnat.parser.*
import agnat.parser.Common.colon
import agnat.parser.Common.comma
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class OptionalTest {

    private val parser = Optional(Capture(COMMA), NoOp)

    @Test
    fun `should return the parser's result if the input matches it`() {
        val parseState = ParseState.of(comma)
        val result = parser.parse(parseState)
        assertThat(result).hasParsedFully().withResult(comma)
    }

    @Test
    fun `should advance if the input matches the parser`() {
        val parseState = ParseState.of(comma)
        val result = parser.parse(parseState)
        assertThat(result).hasParsedFully().advancingFrom(parseState)
    }

    @Test
    fun `should return the fallback value if the input does not match the parser`() {
        val parseState = ParseState.of(colon)
        val result = parser.parse(parseState)
        assertThat(result).isSuccess().withResult(NoOp)
    }

    @Test
    fun `should not advance if the input does not match the parser`() {
        val parseState = ParseState.of(colon)
        val result = parser.parse(parseState)
        assertThat(result).isSuccess().withoutAdvancingFrom(parseState)
    }
}

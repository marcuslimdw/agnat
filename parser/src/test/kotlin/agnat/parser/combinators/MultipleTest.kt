package agnat.parser.combinators

import agnat.lexer.Token
import agnat.lexer.TokenType.COLON
import agnat.lexer.TokenType.COMMA
import agnat.parser.*
import agnat.parser.Common.comma
import agnat.parser.Common.plus
import agnat.parser.failure.UnexpectedToken
import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

internal class MultipleTest {

    @Test
    fun `should return results from iteratively applying a parser to the input until it stops matching`() {
        val parser = Multiple(Capture(COMMA))
        val parseState = ParseState.of(comma, comma, comma)
        val actual = parser.parse(parseState)
        val expected = listOf(comma, comma, comma)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should advance by a number of tokens equal to the times the input successively matched`() {
        val parser = Multiple(Capture(COMMA))
        val parseState = ParseState.of(comma, comma, comma, plus)
        val actual = parser.parse(parseState)
        assertThat(actual).isSuccess().advancingFrom(parseState).isEqualTo(2)
    }

    @Test
    fun `should return a Success even with 0 matches if the minimum number of matches is 0`() {
        val parser = Multiple(Capture(COLON))
        val parseState = ParseState.of(comma, comma, comma)
        val actual = parser.parse(parseState)
        val expected = emptyList<Token>()
        assertThat(actual).isSuccess().withResult(expected)
    }

    @Test
    fun `should pass through the Failure of the wrapped parser if the number of matches is less than the minimum`() {
        val parser = Multiple(Capture(COMMA), 2)
        val parseState = ParseState.of(comma)
        val actual = parser.parse(parseState)
        val expected = UnexpectedToken(COMMA, parseState.copy(position = 1))
        assertThat(actual).isFailureOf(expected)
    }
}

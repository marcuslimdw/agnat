package agnat.parser.combinators

import agnat.lexer.TokenType.*
import agnat.parser.Common.colon
import agnat.parser.Common.comma
import agnat.parser.Common.newline
import agnat.parser.Common.period
import agnat.parser.Common.plus
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class CommaSeparatedTest {

    private val parser = CommaSeparated(Consume(COLON), Capture(PERIOD), Consume(PLUS))

    @Test
    fun `should parse comma-separated elements with a newline after the opening delimiter`() {
        val parseState = ParseState.of(colon, newline, newline, newline, period, comma, period, plus)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(listOf(period, period))
    }

    @Test
    fun `should parse comma-separated elements with a newline before the closing delimiter`() {
        val parseState = ParseState.of(colon, period, comma, period, newline, newline, newline, plus)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(listOf(period, period))
    }

    @Test
    fun `should parse comma-separated elements with a newline between elements`() {
        val parseState = ParseState.of(colon, period, newline, comma, newline, newline, period, comma, period, plus)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(listOf(period, period, period))
    }

    @Test
    fun `should parse comma-separated elements with a trailing comma`() {
        val parseState = ParseState.of(colon, period, comma, period, comma, period, newline, newline, comma, plus)
        val actual = parser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(listOf(period, period, period))
    }
}

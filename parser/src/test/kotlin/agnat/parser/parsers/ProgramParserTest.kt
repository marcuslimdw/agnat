package agnat.parser.parsers

import agnat.ast.node.BinaryExpression
import agnat.ast.node.Call
import agnat.ast.node.UntypedValueIdentifier
import agnat.ast.node.VariableDeclaration
import agnat.ast.operator.BinaryOperator.ADD
import agnat.ast.operator.BinaryOperator.MULTIPLY
import agnat.lexer.TokenType.*
import agnat.parser.Common.asterisk
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.newline
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.plus
import agnat.parser.Common.testToken
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.failure.ExpectedNewline
import agnat.parser.hasParsedFully
import agnat.parser.isFailureOf
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ProgramParserTest {

    @Test
    fun `should parse a program with newlines separating statements`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "="),
            oneToken,
            newline,
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "y"),
            testToken(EQUAL, "="),
            twoToken,
            newline,
            newline,
            testToken(IDENTIFIER, "print"),
            openParenthesis,
            testToken(IDENTIFIER, "x"),
            plus,
            testToken(IDENTIFIER, "y"),
            asterisk,
            threeToken,
            closeParenthesis,
        )
        val actual = ProgramParser.parse(parseState)
        val expected = listOf(
            VariableDeclaration(UntypedValueIdentifier("x"), one),
            VariableDeclaration(UntypedValueIdentifier("y"), two),
            Call(
                UntypedValueIdentifier("print"),
                listOf(
                    BinaryExpression(
                        ADD,
                        UntypedValueIdentifier("x"),
                        BinaryExpression(
                            MULTIPLY,
                            UntypedValueIdentifier("y"),
                            three
                        )
                    )
                )
            )
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should return ExpectedNewline when two statements are not separated by a newline`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "="),
            oneToken,
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "y"),
            testToken(EQUAL, "="),
            twoToken,
        )
        val actual = ProgramParser.parse(parseState)
        assertThat(actual).isFailureOf(ExpectedNewline(parseState.copy(position = 4)))
    }
}

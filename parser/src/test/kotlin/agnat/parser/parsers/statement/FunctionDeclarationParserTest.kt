package agnat.parser.parsers.statement

import agnat.ast.node.*
import agnat.ast.operator.BinaryOperator.ADD
import agnat.lexer.TokenType.*
import agnat.parser.Common.closeBrace
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.colon
import agnat.parser.Common.comma
import agnat.parser.Common.newline
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openBrace
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.plus
import agnat.parser.Common.testToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.isFailure
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class FunctionDeclarationParserTest {

    @Test
    fun `should parse the declaration of a function with no parameters or return type annotation`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            emptyList(),
            Block(listOf(one)),
            TypeIdentifier("Unit")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function with parameters without a return type annotation`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "A"),
            comma,
            testToken(IDENTIFIER, "b"),
            colon,
            testToken(IDENTIFIER, "B"),
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val parameters = listOf(
            TypedValueIdentifier("a", TypeIdentifier("A")),
            TypedValueIdentifier("b", TypeIdentifier("B"))
        )
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            parameters,
            Block(listOf(one)),
            TypeIdentifier("Unit")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function without parameters with a return type annotation`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            closeParenthesis,
            colon,
            testToken(IDENTIFIER, "Int"),
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            emptyList(),
            Block(listOf(one)),
            TypeIdentifier("Int")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function with parameters and a return type annotation`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "A"),
            comma,
            testToken(IDENTIFIER, "b"),
            colon,
            testToken(IDENTIFIER, "B"),
            closeParenthesis,
            colon,
            testToken(IDENTIFIER, "Int"),
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val parameters = listOf(
            TypedValueIdentifier("a", TypeIdentifier("A")),
            TypedValueIdentifier("b", TypeIdentifier("B"))
        )
        val expected =
            FunctionDeclaration(UntypedValueIdentifier("f"), parameters, Block(listOf(one)), TypeIdentifier("Int"))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function without a return type annotation or a block body`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "A"),
            closeParenthesis,
            testToken(EQUAL, "="),
            oneToken,
            plus,
            twoToken
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val parameters = listOf(TypedValueIdentifier("a", TypeIdentifier("A")))
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            parameters,
            BinaryExpression(ADD, one, two),
            TypeIdentifier("Unit")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function with a return type annotation without a block body`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "A"),
            closeParenthesis,
            colon,
            testToken(IDENTIFIER, "Int"),
            testToken(EQUAL, "="),
            oneToken,
            plus,
            twoToken
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val parameters = listOf(TypedValueIdentifier("a", TypeIdentifier("A")))
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            parameters,
            BinaryExpression(ADD, one, two),
            TypeIdentifier("Int")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function with parameters without a trailing comma`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "A"),
            comma,
            testToken(IDENTIFIER, "b"),
            colon,
            testToken(IDENTIFIER, "B"),
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val parameters = listOf(
            TypedValueIdentifier("a", TypeIdentifier("A")),
            TypedValueIdentifier("b", TypeIdentifier("B"))
        )
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            parameters,
            Block(listOf(one)),
            TypeIdentifier("Unit")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function with parameters with a trailing comma`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "A"),
            comma,
            testToken(IDENTIFIER, "b"),
            colon,
            testToken(IDENTIFIER, "B"),
            comma,
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val parameters = listOf(
            TypedValueIdentifier("a", TypeIdentifier("A")),
            TypedValueIdentifier("b", TypeIdentifier("B"))
        )
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            parameters,
            Block(listOf(one)),
            TypeIdentifier("Unit")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse the declaration of a function with a final return`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            closeParenthesis,
            colon,
            testToken(IDENTIFIER, "Int"),
            testToken(EQUAL, "="),
            openBrace,
            newline,
            testToken(RETURN, "return"),
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        val expected = FunctionDeclaration(
            UntypedValueIdentifier("f"),
            emptyList(),
            Block(listOf(Return(IntLiteral(1)))),
            TypeIdentifier("Int")
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should fail to parse a function declaration without parameter type annotations`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            testToken(IDENTIFIER, "a"),
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        assertThat(actual).isFailure()
    }

    @Test
    fun `should fail to parse a function declaration with a name containing an initial capital letter`() {
        val parseState = ParseState.of(
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "F"),
            openParenthesis,
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            newline,
            oneToken,
            newline,
            closeBrace
        )
        val actual = FunctionDeclarationParser.parse(parseState)
        assertThat(actual).isFailure()
    }
}

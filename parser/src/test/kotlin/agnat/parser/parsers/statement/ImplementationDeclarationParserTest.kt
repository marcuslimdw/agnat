package agnat.parser.parsers.statement

import agnat.ast.node.*
import agnat.lexer.TokenType.*
import agnat.parser.Common.closeBrace
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.colon
import agnat.parser.Common.newline
import agnat.parser.Common.one
import agnat.parser.Common.openBrace
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.isFailure
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ImplementationDeclarationParserTest {

    @Test
    fun `should parse an implementation declaration without members or braces`() {
        val parseState = ParseState.of(
            testToken(IMPLEMENTATION, "implementation"),
            testToken(IDENTIFIER, "Implementation"),
        )
        val actual = ImplementationDeclarationParser.parse(parseState)
        val expected = ImplementationDeclaration(TypeIdentifier("Implementation"), emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse an implementation declaration without members with braces`() {
        val parseState = ParseState.of(
            testToken(IMPLEMENTATION, "implementation"),
            testToken(IDENTIFIER, "Implementation"),
            openBrace,
            newline,
            closeBrace
        )
        val actual = ImplementationDeclarationParser.parse(parseState)
        val expected = ImplementationDeclaration(TypeIdentifier("Implementation"), emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse an implementation declaration with members`() {
        val parseState = ParseState.of(
            testToken(IMPLEMENTATION, "implementation"),
            testToken(IDENTIFIER, "Implementation"),
            openBrace,
            newline,
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "f"),
            openParenthesis,
            closeParenthesis,
            colon,
            testToken(IDENTIFIER, "Int"),
            testToken(EQUAL, "="),
            testToken(INT_LITERAL, "1"),
            newline,
            newline,
            testToken(DEF, "def"),
            testToken(IDENTIFIER, "g"),
            openParenthesis,
            testToken(IDENTIFIER, "value"),
            colon,
            testToken(IDENTIFIER, "Int"),
            closeParenthesis,
            testToken(EQUAL, "="),
            openBrace,
            closeBrace,
            newline,
            closeBrace
        )
        val actual = ImplementationDeclarationParser.parse(parseState)
        val parameters = listOf(TypedValueIdentifier("value", TypeIdentifier("Int")))
        val members = listOf(
            FunctionDeclaration(UntypedValueIdentifier("f"), emptyList(), one, TypeIdentifier("Int")),
            FunctionDeclaration(UntypedValueIdentifier("g"), parameters, Block(emptyList()), TypeIdentifier("Unit"))
        )
        val expected = ImplementationDeclaration(TypeIdentifier("Implementation"), members)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should fail to parse an implementation declaration with a name containing an initial small letter`() {
        val parseState = ParseState.of(
            testToken(IMPLEMENTATION, "implementation"),
            testToken(IDENTIFIER, "implementation")
        )
        val actual = ImplementationDeclarationParser.parse(parseState)
        assertThat(actual).isFailure()
    }
}

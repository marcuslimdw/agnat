package agnat.parser.parsers.statement

import agnat.ast.node.*
import agnat.lexer.TokenType.*
import agnat.parser.*
import agnat.parser.Common.colon
import agnat.parser.Common.testToken
import agnat.parser.failure.ExpectedExpression
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class VariableDeclarationParserTest {

    @Test
    fun `should parse a variable declaration with a type annotation`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            colon,
            testToken(IDENTIFIER, "Int"),
            testToken(EQUAL, "="),
            testToken(INT_LITERAL, "1")
        )
        val actual = VariableDeclarationParser.parse(parseState)
        val expected = VariableDeclaration(TypedValueIdentifier("x", TypeIdentifier("Int")), IntLiteral(1))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a variable declaration without a type annotation`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "="),
            testToken(INT_LITERAL, "1")
        )
        val actual = VariableDeclarationParser.parse(parseState)
        val expected = VariableDeclaration(UntypedValueIdentifier("x"), IntLiteral(1))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should return ExpectedExpression if an expression to be assigned was not found`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "=")
        )
        val actual = VariableDeclarationParser.parse(parseState)
        assertThat(actual).isFailureOf(ExpectedExpression(parseState.toEnd()))
    }

    @Test
    fun `should return ExpectedExpression if a statement was found instead of an expression to be assigned`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "="),
            testToken(STRUCT, "struct")
        )
        val actual = VariableDeclarationParser.parse(parseState)
        assertThat(actual).isFailureOf(ExpectedExpression(parseState.copy(position = 3)))
    }

    @Test
    fun `should fail to parse a variable declaration with a name containing an initial capital letter`() {
        val parseState = ParseState.of(
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "X"),
            testToken(EQUAL, "="),
            testToken(STRUCT, "1")
        )
        val actual = VariableDeclarationParser.parse(parseState)
        assertThat(actual).isFailure()
    }
}

package agnat.parser.parsers.statement

import agnat.ast.node.StructDeclaration
import agnat.ast.node.TypeIdentifier
import agnat.ast.node.TypedValueIdentifier
import agnat.lexer.TokenType.IDENTIFIER
import agnat.lexer.TokenType.STRUCT
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.colon
import agnat.parser.Common.comma
import agnat.parser.Common.newline
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.isFailure
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class StructDeclarationParserTest {

    @Test
    fun `should parse a data declaration without members or parentheses`() {
        val parseState = ParseState.of(testToken(STRUCT, "struct"), testToken(IDENTIFIER, "Struct"))
        val actual = StructDeclarationParser.parse(parseState)
        val expected = StructDeclaration(TypeIdentifier("Struct"), emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a data declaration without members with parentheses`() {
        val parseState = ParseState.of(
            testToken(STRUCT, "struct"),
            testToken(IDENTIFIER, "Struct"),
            openParenthesis,
            newline,
            closeParenthesis,
        )
        val actual = StructDeclarationParser.parse(parseState)
        val expected = StructDeclaration(TypeIdentifier("Struct"), emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a data declaration with members`() {
        val parseState = ParseState.of(
            testToken(STRUCT, "struct"),
            testToken(IDENTIFIER, "Struct"),
            openParenthesis,
            newline,
            testToken(IDENTIFIER, "a"),
            colon,
            testToken(IDENTIFIER, "Int"),
            comma,
            newline,
            testToken(IDENTIFIER, "b"),
            colon,
            testToken(IDENTIFIER, "Float"),
            comma,
            newline,
            testToken(IDENTIFIER, "c"),
            colon,
            testToken(IDENTIFIER, "String"),
            newline,
            closeParenthesis
        )
        val actual = StructDeclarationParser.parse(parseState)
        val members = listOf(
            TypedValueIdentifier("a", TypeIdentifier("Int")),
            TypedValueIdentifier("b", TypeIdentifier("Float")),
            TypedValueIdentifier("c", TypeIdentifier("String"))
        )
        val expected = StructDeclaration(TypeIdentifier("Struct"), members)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should fail to parse a struct declaration with a name containing an initial small letter`() {
        val parseState = ParseState.of(testToken(STRUCT, "struct"), testToken(IDENTIFIER, "struct"))
        val actual = StructDeclarationParser.parse(parseState)
        assertThat(actual).isFailure()
    }
}

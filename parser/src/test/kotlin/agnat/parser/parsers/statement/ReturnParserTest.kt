package agnat.parser.parsers.statement

import agnat.ast.node.BinaryExpression
import agnat.ast.node.Return
import agnat.ast.operator.BinaryOperator.ADD
import agnat.lexer.TokenType.RETURN
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.plus
import agnat.parser.Common.testToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ReturnParserTest {

    @Test
    fun `should parse a return statement containing a simple expression`() {
        val parseState = ParseState.of(testToken(RETURN, "return"), oneToken)
        val actual = ReturnParser.parse(parseState)
        val expected = Return(one)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a return statement containing a complex expression`() {
        val parseState = ParseState.of(testToken(RETURN, "return"), oneToken, plus, twoToken)
        val actual = ReturnParser.parse(parseState)
        val expected = Return(BinaryExpression(ADD, one, two))
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

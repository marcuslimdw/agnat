package agnat.parser.parsers.statement

import agnat.ast.node.EnumDeclaration
import agnat.ast.node.EnumDeclaration.EnumMemberDeclaration
import agnat.ast.node.TypeIdentifier
import agnat.ast.node.TypedValueIdentifier
import agnat.lexer.TokenType.ENUM
import agnat.lexer.TokenType.IDENTIFIER
import agnat.parser.Common.closeBrace
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.colon
import agnat.parser.Common.comma
import agnat.parser.Common.newline
import agnat.parser.Common.openBrace
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.isFailure
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class EnumDeclarationParserTest {

    @Test
    fun `should parse an enum declaration without members or braces`() {
        val parseState = ParseState.of(testToken(ENUM, "enum"), testToken(IDENTIFIER, "Enum"))
        val actual = EnumDeclarationParser.parse(parseState)
        val expected = EnumDeclaration(TypeIdentifier("Enum"), emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse an enum declaration without members with braces`() {
        val parseState = ParseState.of(
            testToken(ENUM, "enum"),
            testToken(IDENTIFIER, "Enum"),
            openBrace,
            newline,
            closeBrace
        )
        val actual = EnumDeclarationParser.parse(parseState)
        val expected = EnumDeclaration(TypeIdentifier("Enum"), emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse an enum declaration with members`() {
        val parseState = ParseState.of(
            testToken(ENUM, "enum"),
            testToken(IDENTIFIER, "Option"),
            openBrace,
            newline,
            testToken(IDENTIFIER, "SomeInt"),
            openParenthesis,
            testToken(IDENTIFIER, "value"),
            colon,
            testToken(IDENTIFIER, "Int"),
            closeParenthesis,
            comma,
            newline,
            testToken(IDENTIFIER, "None"),
            newline,
            closeBrace
        )
        val actual = EnumDeclarationParser.parse(parseState)
        val members = listOf(
            EnumMemberDeclaration(
                TypeIdentifier("SomeInt"),
                listOf(TypedValueIdentifier("value", TypeIdentifier("Int")))
            ),
            EnumMemberDeclaration(TypeIdentifier("None"), emptyList()),
        )
        val expected = EnumDeclaration(TypeIdentifier("Option"), members)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should fail to parse an enum declaration with a name containing an initial small letter`() {
        val parseState = ParseState.of(testToken(ENUM, "enum"), testToken(IDENTIFIER, "enum"))
        val actual = EnumDeclarationParser.parse(parseState)
        assertThat(actual).isFailure()
    }
}

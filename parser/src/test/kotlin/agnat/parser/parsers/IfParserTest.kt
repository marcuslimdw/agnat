package agnat.parser.parsers

import agnat.ast.node.IfExpression
import agnat.ast.node.UnitLiteral
import agnat.parser.Common.elseToken
import agnat.parser.Common.falseLiteral
import agnat.parser.Common.falseToken
import agnat.parser.Common.ifToken
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.trueLiteral
import agnat.parser.Common.trueToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class IfParserTest {

    @Test
    fun `should parse an if expression with an implicit else branch returning Unit`() {
        val parseState = ParseState.of(ifToken, trueToken, oneToken)
        val actual = IfParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(
            IfExpression(
                trueLiteral,
                one,
                UnitLiteral
            )
        )
    }

    @Test
    fun `should parse an if expression with an explicit else branch`() {
        val parseState = ParseState.of(ifToken, trueToken, oneToken, elseToken, twoToken)
        val actual = IfParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(IfExpression(trueLiteral, one, two))
    }

    @Test
    fun `should parse a nested if expression`() {
        val parseState = ParseState.of(
            ifToken, trueToken, ifToken, falseToken, oneToken, elseToken, twoToken, elseToken, threeToken
        )
        val actual = IfParser.parse(parseState)
        val expected = IfExpression(
            trueLiteral,
            IfExpression(falseLiteral, one, two),
            three
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

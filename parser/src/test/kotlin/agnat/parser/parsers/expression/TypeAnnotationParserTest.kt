package agnat.parser.parsers.expression

import agnat.ast.node.TypeIdentifier
import agnat.lexer.TokenType.IDENTIFIER
import agnat.parser.Common.colon
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class TypeAnnotationParserTest {

    @Test
    fun `should parse a type annotation`() {
        val parseState = ParseState.of(colon, testToken(IDENTIFIER, "Int"))
        val actual = TypeAnnotationParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(TypeIdentifier("Int"))
    }
}

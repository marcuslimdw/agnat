package agnat.parser.parsers.expression.call

import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.comma
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ArgumentParserTest {

    @Test
    fun `should parse an empty argument list`() {
        val parseState = ParseState.of(openParenthesis, closeParenthesis)
        val actual = ArgumentParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(emptyList())
    }

    @Test
    fun `should parse a nonempty argument list without a trailing comma`() {
        val parseState = ParseState.of(openParenthesis, oneToken, comma, twoToken, closeParenthesis)
        val actual = ArgumentParser.parse(parseState)
        val expected = listOf(one, two)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a nonempty argument list with a trailing comma`() {
        val parseState = ParseState.of(openParenthesis, oneToken, comma, twoToken, comma, closeParenthesis)
        val actual = ArgumentParser.parse(parseState)
        val expected = listOf(one, two)
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

package agnat.parser.parsers.expression.call

import agnat.ast.node.Call
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class CallParserTest {

    @Test
    fun `should parse a single call`() {
        val parseState = ParseState.of(oneToken, openParenthesis, twoToken, closeParenthesis)
        val actual = CallParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(Call(one, listOf(two)))
    }

    @Test
    fun `should parse chained calls`() {
        val open = openParenthesis
        val close = closeParenthesis
        val parseState = ParseState.of(oneToken, open, twoToken, close, open, threeToken, close)
        val actual = CallParser.parse(parseState)
        val expected = Call(Call(one, listOf(two)), listOf(three))
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

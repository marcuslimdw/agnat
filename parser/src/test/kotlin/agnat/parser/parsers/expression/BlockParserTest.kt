package agnat.parser.parsers.expression

import agnat.ast.node.Block
import agnat.ast.node.UntypedValueIdentifier
import agnat.ast.node.VariableDeclaration
import agnat.lexer.TokenType.*
import agnat.parser.Common.closeBrace
import agnat.parser.Common.newline
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openBrace
import agnat.parser.Common.testToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class BlockParserTest {

    @Test
    fun `should parse an empty block`() {
        val parseState = ParseState.of(openBrace, newline, closeBrace)
        val actual = BlockParser.parse(parseState)
        val expected = Block(emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a block containing statements and expressions`() {
        val parseState = ParseState.of(
            openBrace,
            newline,
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "="),
            oneToken,
            newline,
            twoToken,
            newline,
            closeBrace
        )
        val actual = BlockParser.parse(parseState)
        val expectedNodes = listOf(VariableDeclaration(UntypedValueIdentifier("x"), one), two)
        val expected = Block(expectedNodes)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a block containing only statements`() {
        val parseState = ParseState.of(
            openBrace,
            newline,
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "x"),
            testToken(EQUAL, "="),
            oneToken,
            newline,
            testToken(VAL, "val"),
            testToken(IDENTIFIER, "y"),
            testToken(EQUAL, "="),
            twoToken,
            newline,
            closeBrace
        )
        val actual = BlockParser.parse(parseState)
        val expectedNodes = listOf(
            VariableDeclaration(UntypedValueIdentifier("x"), one),
            VariableDeclaration(UntypedValueIdentifier("y"), two)
        )
        val expected = Block(expectedNodes)
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

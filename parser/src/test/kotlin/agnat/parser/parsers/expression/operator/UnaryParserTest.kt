package agnat.parser.parsers.expression.operator

import agnat.ast.node.IntLiteral
import agnat.ast.node.UnaryExpression
import agnat.ast.operator.UnaryOperator.ARITHMETIC_NEGATE
import agnat.ast.operator.UnaryOperator.LOGICAL_NEGATE
import agnat.lexer.TokenType.BANG
import agnat.lexer.TokenType.INT_LITERAL
import agnat.parser.Common.minus
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class UnaryParserTest {

    @Test
    fun `unary should parse a single unary operator followed by a literal`() {
        val parseState = ParseState.of(minus, testToken(INT_LITERAL, "1"))
        val actual = UnaryParser.parse(parseState)
        val expected = UnaryExpression(ARITHMETIC_NEGATE, IntLiteral(1))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `unary should parse multiple unary operators followed by a literal`() {
        val parseState = ParseState.of(minus, minus, testToken(BANG, "!"), testToken(INT_LITERAL, "1"))
        val actual = UnaryParser.parse(parseState)
        val expected = UnaryExpression(
            ARITHMETIC_NEGATE,
            UnaryExpression(
                ARITHMETIC_NEGATE,
                UnaryExpression(LOGICAL_NEGATE, IntLiteral(1))
            )
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

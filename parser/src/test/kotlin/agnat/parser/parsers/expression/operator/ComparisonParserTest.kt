package agnat.parser.parsers.expression.operator

import agnat.ast.node.BinaryExpression
import agnat.ast.operator.BinaryOperator.ADD
import agnat.ast.operator.BinaryOperator.COMPARE_EQUAL
import agnat.lexer.TokenType.EQUAL_EQUAL
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.plus
import agnat.parser.Common.testToken
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ComparisonParserTest {

    @Test
    fun `should parse expressions separated by a comparison operator`() {
        val parseState = ParseState.of(oneToken, testToken(EQUAL_EQUAL, "=="), twoToken)
        val actual = ComparisonParser.parse(parseState)
        val expected = BinaryExpression(COMPARE_EQUAL, one, two)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse expressions separated by a comparison operator including a term operator`() {
        val parseState = ParseState.of(oneToken, testToken(EQUAL_EQUAL, "=="), twoToken, plus, threeToken)
        val actual = ComparisonParser.parse(parseState)
        val expected = BinaryExpression(
            COMPARE_EQUAL,
            one,
            BinaryExpression(ADD, two, three)
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

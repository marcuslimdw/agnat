package agnat.parser.parsers.expression.literal

import agnat.ast.node.BooleanLiteral
import agnat.ast.node.FloatLiteral
import agnat.ast.node.IntLiteral
import agnat.ast.node.StringLiteral
import agnat.lexer.TokenType.*
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class SimpleLiteralParserTest {

    @Test
    fun `should parse an int literal`() {
        val parseState = ParseState.of(testToken(INT_LITERAL, "1"))
        val actual = SimpleLiteralParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(IntLiteral(1))
    }

    @Test
    fun `should parse a float literal`() {
        val parseState = ParseState.of(testToken(FLOAT_LITERAL, "1.0"))
        val actual = SimpleLiteralParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(FloatLiteral(1.0))
    }

    @Test
    fun `should parse a string literal`() {
        val parseState = ParseState.of(testToken(STRING_LITERAL, "abcdef"))
        val actual = SimpleLiteralParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(StringLiteral("abcdef"))
    }

    @Test
    fun `should parse a true literal`() {
        val parseState = ParseState.of(testToken(TRUE_LITERAL, "true"))
        val actual = SimpleLiteralParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(BooleanLiteral(true))
    }

    @Test
    fun `should parse a false literal`() {
        val parseState = ParseState.of(testToken(FALSE_LITERAL, "false"))
        val actual = SimpleLiteralParser.parse(parseState)
        assertThat(actual).hasParsedFully().withResult(BooleanLiteral(false))
    }
}

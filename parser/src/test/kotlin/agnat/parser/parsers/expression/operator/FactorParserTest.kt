package agnat.parser.parsers.expression.operator

import agnat.ast.node.BinaryExpression
import agnat.ast.node.UnaryExpression
import agnat.ast.operator.BinaryOperator.DIVIDE
import agnat.ast.operator.BinaryOperator.MULTIPLY
import agnat.ast.operator.UnaryOperator.ARITHMETIC_NEGATE
import agnat.parser.Common.asterisk
import agnat.parser.Common.forwardSlash
import agnat.parser.Common.minus
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class FactorParserTest {

    @Test
    fun `should parse expressions separated by a factor operator`() {
        val parseState = ParseState.of(oneToken, asterisk, twoToken, forwardSlash, threeToken)
        val actual = FactorParser.parse(parseState)
        val expected = BinaryExpression(
            DIVIDE,
            BinaryExpression(MULTIPLY, one, two),
            three
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse expressions separated by a factor operator including a unary operator`() {
        val parseState = ParseState.of(oneToken, asterisk, minus, twoToken, asterisk, threeToken)
        val actual = FactorParser.parse(parseState)
        val expected = BinaryExpression(
            MULTIPLY,
            BinaryExpression(
                MULTIPLY,
                one,
                UnaryExpression(ARITHMETIC_NEGATE, two)
            ),
            three
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

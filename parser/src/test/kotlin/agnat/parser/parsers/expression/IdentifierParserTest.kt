package agnat.parser.parsers.expression

import agnat.ast.node.TypeIdentifier
import agnat.ast.node.UntypedValueIdentifier
import agnat.lexer.TokenType.IDENTIFIER
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.failure.ExpectedTypeIdentifier
import agnat.parser.failure.ExpectedValueIdentifier
import agnat.parser.hasParsedFully
import agnat.parser.isFailureOf
import agnat.parser.parsers.expression.IdentifierParser.TypeIdentifierParser
import agnat.parser.parsers.expression.IdentifierParser.ValueIdentifierParser
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class IdentifierParserTest {

    @Nested
    inner class `when allowed to parse types` {

        @Test
        fun `should parse an identifier starting with a capital letter`() {
            val parseState = ParseState.of(testToken(IDENTIFIER, "X"))
            val actual = TypeIdentifierParser.parse(parseState)
            assertThat(actual).hasParsedFully().withResult(TypeIdentifier("X"))
        }

        @Test
        fun `should return ExpectedTypeIdentifier given an identifier starting with a small letter`() {
            val parseState = ParseState.of(testToken(IDENTIFIER, "x"))
            val actual = TypeIdentifierParser.parse(parseState)
            val expected = ExpectedTypeIdentifier(parseState)
            assertThat(actual).isFailureOf(expected)
        }
    }

    @Nested
    inner class `when allowed to parse values` {

        @Test
        fun `should parse an identifier starting with a small letter`() {
            val parseState = ParseState.of(testToken(IDENTIFIER, "x"))
            val actual = ValueIdentifierParser.parse(parseState)
            assertThat(actual).hasParsedFully().withResult(UntypedValueIdentifier("x"))
        }

        @Test
        fun `should return ExpectedTypeIdentifier given an identifier starting with a capital letter`() {
            val parseState = ParseState.of(testToken(IDENTIFIER, "X"))
            val actual = ValueIdentifierParser.parse(parseState)
            val expected = ExpectedValueIdentifier(parseState)
            assertThat(actual).isFailureOf(expected)
        }
    }
}

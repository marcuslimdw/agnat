package agnat.parser.parsers.expression.literal

import agnat.ast.node.ListLiteral
import agnat.parser.Common.closeBracket
import agnat.parser.Common.comma
import agnat.parser.Common.newline
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openBracket
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.failure.UnmatchedBracket
import agnat.parser.hasParsedFully
import agnat.parser.toEnd
import agnat.parser.withResult
import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test

internal class ListLiteralParserTest {

    @Test
    fun `should parse an empty list literal`() {
        val parseState = ParseState.of(open, close)
        val actual = ListLiteralParser.parse(parseState)
        val expected = ListLiteral(emptyList())
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a list literal containing elements without a trailing comma`() {
        val parseState = ParseState.of(open, oneToken, comma, twoToken, close)
        val actual = ListLiteralParser.parse(parseState)
        val expected = ListLiteral(listOf(one, two))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a list literal containing elements with a trailing comma`() {
        val parseState = ParseState.of(open, oneToken, comma, twoToken, comma, close)
        val actual = ListLiteralParser.parse(parseState)
        val expected = ListLiteral(listOf(one, two))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a list literal containing newlines`() {
        val parseState = ParseState.of(
            open, newline, newline, oneToken, comma, newline, newline, twoToken, newline, comma, newline, close
        )
        val actual = ListLiteralParser.parse(parseState)
        val expected = ListLiteral(listOf(one, two))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a list literal containing a list literal`() {
        val parseState = ParseState.of(open, oneToken, comma, open, twoToken, close, comma, oneToken, close)
        val actual = ListLiteralParser.parse(parseState)
        val expected = ListLiteral(listOf(one, ListLiteral(listOf(two)), one))
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should return UnmatchedBracket if no closing bracket was found without a trailing comma`() {
        val parseState = ParseState.of(open, oneToken, comma, twoToken)
        val actual = ListLiteralParser.parse(parseState)
        val expected = UnmatchedBracket(parseState.toEnd())
        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `should return UnmatchedBracket if no closing bracket was found with a trailing comma`() {
        val parseState = ParseState.of(open, oneToken, comma, twoToken, comma)
        val actual = ListLiteralParser.parse(parseState)
        val expected = UnmatchedBracket(parseState.toEnd())
        assertThat(actual).isEqualTo(expected)
    }

    companion object {

        private val open = openBracket
        private val close = closeBracket
    }
}

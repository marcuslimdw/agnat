package agnat.parser.parsers.expression

import agnat.ast.node.BinaryExpression
import agnat.ast.operator.BinaryOperator.ADD
import agnat.ast.operator.BinaryOperator.MULTIPLY
import agnat.parser.*
import agnat.parser.Common.asterisk
import agnat.parser.Common.closeParenthesis
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.openParenthesis
import agnat.parser.Common.plus
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.failure.UnmatchedParenthesis
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class ParenthesisedParserTest {

    @Test
    fun `should parse a grouping containing an expression`() {
        val parseState = ParseState.of(open, oneToken, plus, twoToken, close)
        val actual = ParenthesisedParser.parse(parseState)
        val expected = BinaryExpression(ADD, one, two)
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse a grouping containing a grouping`() {
        val parseState = ParseState.of(
            open, threeToken, asterisk, open, twoToken, plus, oneToken, close, plus, oneToken, close
        )
        val actual = ParenthesisedParser.parse(parseState)
        val expected = BinaryExpression(
            ADD,
            BinaryExpression(
                MULTIPLY,
                three,
                BinaryExpression(ADD, two, one)
            ),
            one
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should return UnmatchedParenthesis if no closing parenthesis was found`() {
        val parseState = ParseState.of(open, oneToken, asterisk, twoToken)
        val actual = ParenthesisedParser.parse(parseState)
        assertThat(actual).isFailureOf(UnmatchedParenthesis(parseState.toEnd()))
    }

    companion object {
        val open = openParenthesis
        val close = closeParenthesis
    }
}

package agnat.parser.parsers.expression.operator

import agnat.ast.node.BinaryExpression
import agnat.ast.operator.BinaryOperator.*
import agnat.parser.Common.asterisk
import agnat.parser.Common.minus
import agnat.parser.Common.one
import agnat.parser.Common.oneToken
import agnat.parser.Common.plus
import agnat.parser.Common.three
import agnat.parser.Common.threeToken
import agnat.parser.Common.two
import agnat.parser.Common.twoToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class TermParserTest {

    @Test
    fun `should parse expressions separated by a term operator`() {
        val parseState = ParseState.of(oneToken, minus, twoToken, plus, threeToken)
        val actual = TermParser.parse(parseState)
        val expected = BinaryExpression(
            ADD,
            BinaryExpression(SUBTRACT, one, two),
            three
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse expressions separated by a term operator including a factor operator`() {
        val parseState = ParseState.of(oneToken, plus, twoToken, asterisk, threeToken)
        val actual = TermParser.parse(parseState)
        val expected = BinaryExpression(
            ADD,
            one,
            BinaryExpression(MULTIPLY, two, three)
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

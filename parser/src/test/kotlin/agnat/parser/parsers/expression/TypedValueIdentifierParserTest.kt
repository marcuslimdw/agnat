package agnat.parser.parsers.expression

import agnat.ast.node.TypeIdentifier
import agnat.ast.node.TypedValueIdentifier
import agnat.lexer.TokenType.COLON
import agnat.lexer.TokenType.IDENTIFIER
import agnat.parser.Common.testToken
import agnat.parser.ParseState
import agnat.parser.failure.ExpectedTypeAnnotation
import agnat.parser.isFailureOf
import agnat.parser.isSuccess
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class TypedValueIdentifierParserTest {

    @Test
    fun `should parse a value identifier with type annotation`() {
        val parseState = ParseState.of(testToken(IDENTIFIER, "x"), testToken(COLON, ":"), testToken(IDENTIFIER, "X"))
        val actual = TypedValueIdentifierParser.parse(parseState)
        val expected = TypedValueIdentifier("x", TypeIdentifier("X"))
        assertThat(actual).isSuccess().withResult(expected)
    }

    @Test
    fun `should return ExpectedTypeAnnotation when only a raw value identifier was found`() {
        val parseState = ParseState.of(testToken(IDENTIFIER, "x"))
        val actual = TypedValueIdentifierParser.parse(parseState)
        val expected = ExpectedTypeAnnotation(parseState)
        assertThat(actual).isFailureOf(expected)
    }
}

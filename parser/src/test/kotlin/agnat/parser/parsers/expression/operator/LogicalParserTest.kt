package agnat.parser.parsers.expression.operator

import agnat.ast.node.BinaryExpression
import agnat.ast.operator.BinaryOperator.*
import agnat.lexer.TokenType.EQUAL_EQUAL
import agnat.parser.Common.ampersand
import agnat.parser.Common.bar
import agnat.parser.Common.falseLiteral
import agnat.parser.Common.falseToken
import agnat.parser.Common.testToken
import agnat.parser.Common.trueLiteral
import agnat.parser.Common.trueToken
import agnat.parser.ParseState
import agnat.parser.hasParsedFully
import agnat.parser.withResult
import assertk.assertThat
import org.junit.jupiter.api.Test

internal class LogicalParserTest {

    @Test
    fun `should parse expressions separated by a bitwise operator`() {
        val parseState = ParseState.of(trueToken, bar, falseToken, ampersand, trueToken)
        val actual = LogicalParser.parse(parseState)
        val expected = BinaryExpression(
            LOGICAL_AND, BinaryExpression(LOGICAL_OR, trueLiteral, falseLiteral), trueLiteral
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }

    @Test
    fun `should parse expressions separated by a bitwise operator including a comparison operator`() {
        val parseState = ParseState.of(trueToken, bar, falseToken, testToken(EQUAL_EQUAL, "=="), trueToken)
        val actual = LogicalParser.parse(parseState)
        val expected = BinaryExpression(
            LOGICAL_OR,
            trueLiteral,
            BinaryExpression(COMPARE_EQUAL, falseLiteral, trueLiteral)
        )
        assertThat(actual).hasParsedFully().withResult(expected)
    }
}

package agnat.parser

import agnat.ast.node.BooleanLiteral
import agnat.ast.node.IntLiteral
import agnat.lexer.Position
import agnat.lexer.Token
import agnat.lexer.TokenType
import agnat.lexer.TokenType.*

internal object Common {

    val newline = testToken(NEWLINE, "\n")

    val openParenthesis = testToken(OPEN_PARENTHESIS, "(")
    val closeParenthesis = testToken(CLOSE_PARENTHESIS, ")")
    val openBracket = testToken(OPEN_BRACKET, "[")
    val closeBracket = testToken(CLOSE_BRACKET, "]")
    val openBrace = testToken(OPEN_BRACE, "{")
    val closeBrace = testToken(CLOSE_BRACE, "}")

    val ifToken = testToken(IF, "if")
    val elseToken = testToken(ELSE, "else")

    val colon = testToken(COLON, ":")
    val comma = testToken(COMMA, ",")
    val period = testToken(PERIOD, ".")

    val plus = testToken(PLUS, "+")
    val minus = testToken(MINUS, "-")
    val asterisk = testToken(ASTERISK, "*")
    val forwardSlash = testToken(FORWARD_SLASH, "/")

    val ampersand = testToken(AMPERSAND, "&")
    val bar = testToken(BAR, "|")

    val trueToken = testToken(TRUE_LITERAL, "true")
    val falseToken = testToken(FALSE_LITERAL, "false")

    val oneToken = testToken(INT_LITERAL, "1")
    val twoToken = testToken(INT_LITERAL, "2")
    val threeToken = testToken(INT_LITERAL, "3")

    val trueLiteral = BooleanLiteral(true)
    val falseLiteral = BooleanLiteral(false)

    val one = IntLiteral(1)
    val two = IntLiteral(2)
    val three = IntLiteral(3)

    fun testToken(tokenType: TokenType, lexeme: String) = Token(tokenType, lexeme, Position.initial)
}

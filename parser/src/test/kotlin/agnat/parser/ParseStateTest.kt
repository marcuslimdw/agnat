package agnat.parser

import agnat.lexer.Position
import agnat.lexer.Token
import agnat.lexer.TokenType.*
import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class ParseStateTest {

    @Nested
    inner class `of should` {

        @Test
        fun `create a parse state containing tokens with an EOF token appended at the position of the last token`() {
            val parseState = ParseState.of(
                Token(VAL, "val", Position(0, 0)),
                Token(IDENTIFIER, "x", Position(0, 4)),
                Token(EQUAL, "=", Position(0, 6)),
                Token(INT_LITERAL, "1", Position(0, 8))
            )
            val expected = Token(EOF, "", Position(0, 8))
            assertThat(parseState.source.lastOrNull()).isNotNull().isEqualTo(expected)
        }

        @Test
        fun `create an empty parse state with an EOF token appended at the initial position`() {
            val parseState = ParseState.of()
            val expected = Token(EOF, "", Position.initial)
            assertThat(parseState.source.lastOrNull()).isNotNull().isEqualTo(expected)

        }
    }
}

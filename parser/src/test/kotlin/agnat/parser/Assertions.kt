package agnat.parser

import agnat.lexer.TokenType
import assertk.Assert
import assertk.assertions.isEqualTo
import assertk.fail

internal fun <T> Assert<ParseResult<T>>.isSuccess(): Assert<Success<T>> = transform { actual ->
    if (actual !is Success) fail("Expected a Success but was ${actual::class}.")
    actual
}

internal fun Assert<ParseResult<*>>.isFailure(): Assert<ParseFailure> = transform { actual ->
    if (actual !is ParseFailure) fail("Expected a Failure but was ${actual::class}.")
    actual
}

internal fun Assert<ParseResult<*>>.isFailureOf(expected: ParseFailure) = given { actual ->
    if (actual !is ParseFailure) fail("Expected a Failure but was ${actual::class}.")
    isEqualTo(expected)
}

internal fun <T> Assert<ParseResult<T>>.hasParsedFully(): Assert<Success<T>> = transform { actual ->
    when (actual) {
        is Success -> {
            val parseState = actual.newParseState
            if (parseState.current().tokenType != TokenType.EOF)
                fail("Expected to be done parsing but was at ${parseState.position}/${parseState.source.size}")

            actual
        }
        is ParseFailure -> fail("Expected a Success but was a Failure: ${actual.reason}.")
    }
}

internal fun <T> Assert<Success<T>>.withResult(expected: T) = transform { it.parsed }.isEqualTo(expected)

internal fun <T> Assert<Success<T>>.advancingFrom(expected: ParseState) = transform { actual ->
    val newPosition = actual.newParseState.position
    val expectedPosition = expected.position + 1
    if (newPosition < expectedPosition) fail("Expected a position ahead of $expectedPosition but got $newPosition.")
    newPosition - expectedPosition
}

internal fun <T> Assert<ParseResult<T>>.withoutAdvancingFrom(expected: ParseState) = given { actual ->
    val newPosition = when (actual) {
        is Success -> actual.newParseState.position
        is ParseFailure -> actual.lastGoodState.position
    }
    val expectedPosition = expected.position
    if (newPosition != expectedPosition) fail("Expected a position of $expectedPosition but was $newPosition.")
}

internal fun ParseState.toEnd() = copy(position = source.size - 1)

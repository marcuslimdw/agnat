package agnat.parser

import agnat.lexer.Position
import agnat.lexer.Token
import agnat.lexer.TokenType.EOF

data class ParseState(
    val source: List<Token>,
    val position: Int
) {

    fun current(): Token = source[position]

    fun advance(): ParseState = copy(position = position + 1)

    override fun toString(): String {
        val (done, pending) = when (position) {
            0 -> listOf(emptyList(), source)
            else -> source.chunked(position)
        }
        val doneString = done.joinToString(", ") { it.toString() }
        val pendingString = pending.joinToString(", ") { it.toString() }
        return "[$doneString >> $pendingString]"
    }

    companion object {

        fun of(vararg source: Token): ParseState {
            val eofPosition = source.lastOrNull()?.position ?: Position.initial
            return ParseState(source.toList() + Token(EOF, "", eofPosition), 0)
        }
    }
}

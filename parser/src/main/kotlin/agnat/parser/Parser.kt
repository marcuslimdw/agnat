package agnat.parser

interface Parser<out T> {

    fun parse(state: ParseState): ParseResult<T>
}

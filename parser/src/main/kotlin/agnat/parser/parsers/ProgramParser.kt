package agnat.parser.parsers

import agnat.ast.node.Node
import agnat.lexer.TokenType.EOF
import agnat.lexer.TokenType.NEWLINE
import agnat.parser.*
import agnat.parser.combinators.*
import agnat.parser.failure.ExpectedNewline
import agnat.parser.failure.UnexpectedToken
import agnat.parser.parsers.statement.DeclarationParser
import agnat.util.Cons
import agnat.util.LinkedList
import agnat.util.Nil

internal object ProgramParser : Parser<List<Node>> {

    private val separators = setOf(NEWLINE, EOF)

    override fun parse(state: ParseState): ParseResult<List<Node>> = recursiveParse(Nil, state)

    private tailrec fun recursiveParse(
        accumulator: LinkedList<Node>,
        state: ParseState
    ): ParseResult<List<Node>> {
        val separatorParser = AnyOf(Multiple(Consume(NEWLINE), 1), Lookahead(EOF))
        val separatedDeclarationParser = Dyad.first(DeclarationParser, separatorParser)
        val result = separatedDeclarationParser.parse(state)
        return when {
            result is Success -> {
                val (parsed, nextState) = result
                val nextParsed = Cons(parsed, accumulator)
                when (nextState.current().tokenType) {
                    EOF -> Success(nextParsed.reversed().toList(), nextState)
                    else -> recursiveParse(nextParsed, nextState)
                }
            }
            result is UnexpectedToken && result.expected.intersect(separators).isNotEmpty() ->
                ExpectedNewline(result.lastGoodState)
            else -> result as ParseFailure
        }
    }
}

package agnat.parser.parsers

import agnat.ast.node.Expression
import agnat.ast.node.IfExpression
import agnat.ast.node.UnitLiteral
import agnat.lexer.TokenType.ELSE
import agnat.lexer.TokenType.IF
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Dyad
import agnat.parser.combinators.Optional
import agnat.parser.combinators.Tetrad

internal object IfParser : Parser<Expression> {

    private val elseParser = Optional(Dyad.second(Consume(ELSE), ExpressionParser), UnitLiteral)

    private val bodyParser = Tetrad(
        Consume(IF), ExpressionParser, ExpressionParser, elseParser
    ) { _, condition, trueBranch, falseBranch -> IfExpression(condition, trueBranch, falseBranch) }

    override fun parse(state: ParseState): ParseResult<Expression> = bodyParser.parse(state)
}

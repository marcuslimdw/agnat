package agnat.parser.parsers

import agnat.ast.node.Node
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.AnyOf
import agnat.parser.parsers.statement.ReturnParser

internal object StatementParser : Parser<Node> {

    private val bodyParser = AnyOf(ExpressionParser, ReturnParser)

    override fun parse(state: ParseState): ParseResult<Node> = bodyParser.parse(state)
}

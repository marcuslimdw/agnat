package agnat.parser.parsers

import agnat.ast.node.Expression
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.AnyOf
import agnat.parser.failure.ExpectedExpression
import agnat.parser.parsers.expression.operator.LogicalParser
import agnat.parser.recoverWith

internal object ExpressionParser : Parser<Expression> {

    private val bodyParser = AnyOf(IfParser, LogicalParser)

    override fun parse(state: ParseState): ParseResult<Expression> = bodyParser
        .parse(state)
        .recoverWith { ExpectedExpression(state) }
}

package agnat.parser.parsers.statement

import agnat.ast.node.Statement
import agnat.ast.node.TypedValueIdentifier
import agnat.ast.node.UntypedValueIdentifier
import agnat.ast.node.VariableDeclaration
import agnat.lexer.TokenType.EQUAL
import agnat.lexer.TokenType.VAL
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Optional
import agnat.parser.combinators.Pentad
import agnat.parser.parsers.ExpressionParser
import agnat.parser.parsers.expression.IdentifierParser.ValueIdentifierParser
import agnat.parser.parsers.expression.TypeAnnotationParser

internal object VariableDeclarationParser : Parser<Statement> {

    private val bodyParser = Pentad(
        Consume(VAL), ValueIdentifierParser, Optional(TypeAnnotationParser, null), Consume(EQUAL), ExpressionParser
    ) { _, valueIdentifier, typeIdentifier, _, expression ->
        val name = valueIdentifier.name
        val identifier = typeIdentifier?.let { TypedValueIdentifier(name, it) } ?: UntypedValueIdentifier(name)
        VariableDeclaration(identifier, expression)
    }

    override fun parse(state: ParseState): ParseResult<Statement> = bodyParser.parse(state)
}

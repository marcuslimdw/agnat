package agnat.parser.parsers.statement

import agnat.ast.node.StructDeclaration
import agnat.lexer.TokenType.*
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.CommaSeparated
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Optional
import agnat.parser.combinators.Triad
import agnat.parser.parsers.expression.IdentifierParser
import agnat.parser.parsers.expression.TypedValueIdentifierParser

object StructDeclarationParser : Parser<StructDeclaration> {

    private val membersParser = CommaSeparated(
        Consume(OPEN_PARENTHESIS),
        TypedValueIdentifierParser,
        Consume(CLOSE_PARENTHESIS)
    )

    private val bodyParser = Triad(
        Consume(STRUCT), IdentifierParser.TypeIdentifierParser, Optional(membersParser, emptyList())
    ) { _, identifier, members -> StructDeclaration(identifier, members) }

    override fun parse(state: ParseState): ParseResult<StructDeclaration> = bodyParser.parse(state)
}

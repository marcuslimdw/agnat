package agnat.parser.parsers.statement

import agnat.ast.node.Return
import agnat.lexer.TokenType.RETURN
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Dyad
import agnat.parser.map
import agnat.parser.parsers.ExpressionParser

internal object ReturnParser : Parser<Return> {

    override fun parse(state: ParseState): ParseResult<Return> = Dyad
        .second(Consume(RETURN), ExpressionParser)
        .parse(state)
        .map(::Return)
}

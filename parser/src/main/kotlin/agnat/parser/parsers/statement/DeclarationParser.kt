package agnat.parser.parsers.statement

import agnat.ast.node.Node
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.AnyOf
import agnat.parser.parsers.StatementParser

internal object DeclarationParser : Parser<Node> {

    private val bodyParser = AnyOf(FunctionDeclarationParser, VariableDeclarationParser, StatementParser)

    override fun parse(state: ParseState): ParseResult<Node> = bodyParser.parse(state)
}

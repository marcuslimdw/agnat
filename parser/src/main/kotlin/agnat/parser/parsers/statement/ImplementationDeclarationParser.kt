package agnat.parser.parsers.statement

import agnat.ast.node.ImplementationDeclaration
import agnat.lexer.TokenType.*
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.*
import agnat.parser.parsers.expression.IdentifierParser.TypeIdentifierParser

object ImplementationDeclarationParser : Parser<ImplementationDeclaration> {

    private val membersParser = Triad.second(
        DropNewlinesAfter(Consume(OPEN_BRACE)),
        Multiple(DropNewlinesAfter(FunctionDeclarationParser)),
        Consume(CLOSE_BRACE)
    )

    private val bodyParser = Triad(
        Consume(IMPLEMENTATION), TypeIdentifierParser, Optional(membersParser, emptyList())
    ) { _, identifier, methods -> ImplementationDeclaration(identifier, methods) }

    override fun parse(state: ParseState): ParseResult<ImplementationDeclaration> =
        bodyParser.parse(state)
}

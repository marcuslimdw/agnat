package agnat.parser.parsers.statement

import agnat.ast.node.EnumDeclaration
import agnat.lexer.TokenType.*
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.*
import agnat.parser.parsers.expression.IdentifierParser.TypeIdentifierParser
import agnat.parser.parsers.expression.TypedValueIdentifierParser

object EnumDeclarationParser : Parser<EnumDeclaration> {

    private val variantParser = CommaSeparated(
        Consume(OPEN_PARENTHESIS),
        TypedValueIdentifierParser,
        Consume(CLOSE_PARENTHESIS)
    )

    private val variantsParser = CommaSeparated(
        Consume(OPEN_BRACE),
        Dyad(TypeIdentifierParser, Optional(variantParser, emptyList())) { identifier, members ->
            EnumDeclaration.EnumMemberDeclaration(identifier, members)
        },
        Consume(CLOSE_BRACE)
    )

    private val bodyParser = Triad(
        Consume(ENUM), TypeIdentifierParser, Optional(variantsParser, emptyList())
    ) { _, identifier, members -> EnumDeclaration(identifier, members) }

    override fun parse(state: ParseState): ParseResult<EnumDeclaration> = bodyParser.parse(state)
}

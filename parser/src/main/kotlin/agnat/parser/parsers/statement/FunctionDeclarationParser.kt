package agnat.parser.parsers.statement

import agnat.ast.node.FunctionDeclaration
import agnat.ast.node.TypeIdentifier
import agnat.lexer.TokenType.*
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.CommaSeparated
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Hexad
import agnat.parser.combinators.Optional
import agnat.parser.parsers.ExpressionParser
import agnat.parser.parsers.expression.IdentifierParser.ValueIdentifierParser
import agnat.parser.parsers.expression.TypeAnnotationParser
import agnat.parser.parsers.expression.TypedValueIdentifierParser

internal object FunctionDeclarationParser : Parser<FunctionDeclaration> {

    private val parametersParser = CommaSeparated(
        Consume(OPEN_PARENTHESIS),
        TypedValueIdentifierParser,
        Consume(CLOSE_PARENTHESIS)
    )

    private val bodyParser = Hexad(
        Consume(DEF),
        ValueIdentifierParser,
        parametersParser,
        Optional(TypeAnnotationParser, TypeIdentifier("Unit")),
        Consume(EQUAL),
        ExpressionParser
    ) { _, identifier, parameters, returnType, _, block ->
        FunctionDeclaration(identifier, parameters, block, returnType)
    }

    override fun parse(state: ParseState): ParseResult<FunctionDeclaration> = bodyParser.parse(state)
}

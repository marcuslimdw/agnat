package agnat.parser.parsers.expression

import agnat.ast.node.Identifier
import agnat.ast.node.TypeIdentifier
import agnat.ast.node.UntypedValueIdentifier
import agnat.lexer.Token
import agnat.lexer.TokenType.IDENTIFIER
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.Success
import agnat.parser.failure.ExpectedTypeIdentifier
import agnat.parser.failure.ExpectedValueIdentifier
import agnat.parser.failure.UnexpectedToken

internal sealed class IdentifierParser<T : Identifier> private constructor(
    private val allowed: Allowed<T>
) : Parser<T> {

    override fun parse(state: ParseState): ParseResult<T> = with(state.current()) {
        when (tokenType) {
            IDENTIFIER -> allowed.match(state)
            else -> UnexpectedToken(IDENTIFIER, state)
        }
    }

    internal object ValueIdentifierParser : IdentifierParser<UntypedValueIdentifier>(Value)

    internal object TypeIdentifierParser : IdentifierParser<TypeIdentifier>(Type)
}

private sealed interface Allowed<T : Identifier> {

    fun match(state: ParseState): ParseResult<T>
}

private object Type : Allowed<TypeIdentifier> {

    override fun match(state: ParseState): ParseResult<TypeIdentifier> = with(state.current()) {
        when (isTypeIdentifier()) {
            true -> Success(TypeIdentifier(lexeme), state.advance())
            false -> ExpectedTypeIdentifier(state)
        }
    }
}

private object Value : Allowed<UntypedValueIdentifier> {

    override fun match(state: ParseState): ParseResult<UntypedValueIdentifier> = with(state.current()) {
        when (!isTypeIdentifier()) {
            true -> Success(UntypedValueIdentifier(lexeme), state.advance())
            false -> ExpectedValueIdentifier(state)
        }
    }
}

private fun Token.isTypeIdentifier(): Boolean = lexeme.first().isUpperCase()

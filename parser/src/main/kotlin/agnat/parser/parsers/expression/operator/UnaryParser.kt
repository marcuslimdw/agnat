package agnat.parser.parsers.expression.operator

import agnat.ast.node.Expression
import agnat.ast.node.UnaryExpression
import agnat.ast.operator.UnaryOperator.ARITHMETIC_NEGATE
import agnat.ast.operator.UnaryOperator.LOGICAL_NEGATE
import agnat.lexer.TokenType.BANG
import agnat.lexer.TokenType.MINUS
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.AnyOf
import agnat.parser.combinators.Capture
import agnat.parser.combinators.Dyad
import agnat.parser.parsers.expression.call.CallParser

internal object UnaryParser : Parser<Expression> {

    private val tokenMap = mapOf(
        MINUS to ARITHMETIC_NEGATE,
        BANG to LOGICAL_NEGATE,
    )

    private val repeatedUnaryParser = Dyad(Capture(tokenMap.keys), UnaryParser) { token, expression ->
        UnaryExpression(tokenMap.getValue(token.tokenType), expression)
    }

    private val bodyParser = AnyOf(repeatedUnaryParser, CallParser)

    override fun parse(state: ParseState): ParseResult<Expression> = bodyParser.parse(state)
}

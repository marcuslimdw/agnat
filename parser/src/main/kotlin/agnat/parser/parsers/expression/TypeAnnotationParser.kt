package agnat.parser.parsers.expression

import agnat.ast.node.TypeIdentifier
import agnat.lexer.TokenType.COLON
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Dyad
import agnat.parser.parsers.expression.IdentifierParser.TypeIdentifierParser

internal object TypeAnnotationParser : Parser<TypeIdentifier> {

    override fun parse(state: ParseState): ParseResult<TypeIdentifier> = Dyad
        .second(Consume(COLON), TypeIdentifierParser)
        .parse(state)
}

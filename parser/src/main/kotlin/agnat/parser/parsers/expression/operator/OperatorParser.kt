package agnat.parser.parsers.expression.operator

import agnat.ast.node.BinaryExpression
import agnat.ast.node.Expression
import agnat.ast.operator.BinaryOperator
import agnat.ast.operator.BinaryOperator.*
import agnat.lexer.TokenType
import agnat.lexer.TokenType.*
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Capture
import agnat.parser.combinators.Dyad
import agnat.parser.combinators.Multiple

private interface OperatorParser : Parser<Expression> {

    val tokenMap: Map<TokenType, BinaryOperator>
    val next: Parser<Expression>

    override fun parse(state: ParseState): ParseResult<Expression> =
        Dyad(next, Multiple(Dyad(Capture(tokenMap.keys), next, ::Pair))) { first, rest ->
            rest.fold(first) { acc, (token, next) ->
                BinaryExpression(tokenMap.getValue(token.tokenType), acc, next)
            }
        }.parse(state)
}

internal object LogicalParser : OperatorParser {

    override val tokenMap = mapOf(
        AMPERSAND to LOGICAL_AND,
        BAR to LOGICAL_OR
    )

    override val next = ComparisonParser
}

internal object ComparisonParser : OperatorParser {

    override val tokenMap = mapOf(
        EQUAL_EQUAL to COMPARE_EQUAL,
        BANG_EQUAL to COMPARE_NOT_EQUAL,
        GREATER to COMPARE_GREATER,
        GREATER_EQUAL to COMPARE_GREATER_EQUAL,
        LESS to COMPARE_LESS,
        LESS_EQUAL to COMPARE_LESS_EQUAL
    )
    override val next = TermParser
}

internal object TermParser : OperatorParser {
    override val tokenMap = mapOf(
        PLUS to ADD,
        MINUS to SUBTRACT
    )
    override val next = FactorParser
}

internal object FactorParser : OperatorParser {

    override val tokenMap = mapOf(
        ASTERISK to MULTIPLY,
        FORWARD_SLASH to DIVIDE
    )
    override val next = UnaryParser
}

package agnat.parser.parsers.expression.call

import agnat.ast.node.Call
import agnat.ast.node.Expression
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Dyad
import agnat.parser.combinators.Multiple
import agnat.parser.parsers.expression.PrimaryParser

internal object CallParser : Parser<Expression> {

    private val bodyParser = Dyad(PrimaryParser, Multiple(ArgumentParser)) { primary, calls ->
        calls.fold(primary) { acc, next -> Call(acc, next) }
    }

    override fun parse(state: ParseState): ParseResult<Expression> = bodyParser.parse(state)
}

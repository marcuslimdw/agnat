package agnat.parser.parsers.expression

import agnat.ast.node.Expression
import agnat.lexer.TokenType.CLOSE_PARENTHESIS
import agnat.lexer.TokenType.OPEN_PARENTHESIS
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Consume
import agnat.parser.combinators.Triad
import agnat.parser.failure.UnmatchedParenthesis
import agnat.parser.parsers.ExpressionParser

internal object ParenthesisedParser : Parser<Expression> {

    override fun parse(state: ParseState): ParseResult<Expression> {
        val openParser = Consume(OPEN_PARENTHESIS)
        val closeParser = Consume(CLOSE_PARENTHESIS, ::UnmatchedParenthesis)
        return Triad.second(openParser, ExpressionParser, closeParser).parse(state)
    }
}

package agnat.parser.parsers.expression.call

import agnat.ast.node.Expression
import agnat.lexer.TokenType.CLOSE_PARENTHESIS
import agnat.lexer.TokenType.OPEN_PARENTHESIS
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.CommaSeparated
import agnat.parser.combinators.Consume
import agnat.parser.failure.UnmatchedParenthesis
import agnat.parser.parsers.ExpressionParser

internal object ArgumentParser : Parser<List<Expression>> {

    private val openParser = Consume(OPEN_PARENTHESIS)
    private val closeParser = Consume(CLOSE_PARENTHESIS, ::UnmatchedParenthesis)
    private val bodyParser = CommaSeparated(openParser, ExpressionParser, closeParser)

    override fun parse(state: ParseState): ParseResult<List<Expression>> = bodyParser.parse(state)
}

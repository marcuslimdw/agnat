package agnat.parser.parsers.expression.literal

import agnat.ast.node.Expression
import agnat.ast.node.ListLiteral
import agnat.lexer.TokenType.CLOSE_BRACKET
import agnat.lexer.TokenType.OPEN_BRACKET
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.CommaSeparated
import agnat.parser.combinators.Consume
import agnat.parser.combinators.DropNewlinesAfter
import agnat.parser.failure.UnmatchedBracket
import agnat.parser.map
import agnat.parser.parsers.ExpressionParser

internal object ListLiteralParser : Parser<Expression> {

    private val bodyParser = CommaSeparated(
        DropNewlinesAfter(Consume(OPEN_BRACKET)),
        ExpressionParser,
        Consume(CLOSE_BRACKET, ::UnmatchedBracket)
    )

    override fun parse(state: ParseState): ParseResult<Expression> = bodyParser.parse(state).map(::ListLiteral)
}

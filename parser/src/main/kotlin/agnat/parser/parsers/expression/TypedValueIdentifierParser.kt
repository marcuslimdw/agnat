package agnat.parser.parsers.expression

import agnat.ast.node.TypedValueIdentifier
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Dyad
import agnat.parser.combinators.Optional
import agnat.parser.failure.ExpectedTypeAnnotation
import agnat.parser.invert
import agnat.parser.parsers.expression.IdentifierParser.ValueIdentifierParser

object TypedValueIdentifierParser : Parser<TypedValueIdentifier> {

    private val bodyParser = Dyad(ValueIdentifierParser, Optional(TypeAnnotationParser, null)) { variable, type ->
        type?.let { variable.withType(it) }
    }

    override fun parse(state: ParseState): ParseResult<TypedValueIdentifier> = bodyParser
        .parse(state)
        .invert()
        ?: ExpectedTypeAnnotation(state)
}

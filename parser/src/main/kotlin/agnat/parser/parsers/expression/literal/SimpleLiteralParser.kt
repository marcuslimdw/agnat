package agnat.parser.parsers.expression.literal

import agnat.ast.node.*
import agnat.lexer.TokenType.*
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.Success
import agnat.parser.failure.UnexpectedToken

internal object SimpleLiteralParser : Parser<Literal> {

    override fun parse(state: ParseState): ParseResult<Literal> {
        val (tokenType, lexeme) = state.current()
        return when (tokenType) {
            INT_LITERAL -> IntLiteral(lexeme.toLong())
            FLOAT_LITERAL -> FloatLiteral(lexeme.toDouble())
            STRING_LITERAL -> StringLiteral(lexeme)
            TRUE_LITERAL -> BooleanLiteral(true)
            FALSE_LITERAL -> BooleanLiteral(false)
            else -> null
        }
            ?.let { Success(it, state.advance()) }
            ?: UnexpectedToken(INT_LITERAL, state)
    }
}

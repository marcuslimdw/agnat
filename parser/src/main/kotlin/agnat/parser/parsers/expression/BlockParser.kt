package agnat.parser.parsers.expression

import agnat.ast.node.Block
import agnat.lexer.TokenType.CLOSE_BRACE
import agnat.lexer.TokenType.OPEN_BRACE
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.Consume
import agnat.parser.combinators.DropNewlinesAfter
import agnat.parser.combinators.Multiple
import agnat.parser.combinators.Triad
import agnat.parser.failure.UnmatchedBrace
import agnat.parser.map
import agnat.parser.parsers.statement.DeclarationParser

internal object BlockParser : Parser<Block> {

    private val bodyParser = Triad.second(
        DropNewlinesAfter(Consume(OPEN_BRACE)),
        Multiple(DropNewlinesAfter(DeclarationParser)),
        Consume(CLOSE_BRACE, ::UnmatchedBrace)
    )

    override fun parse(state: ParseState): ParseResult<Block> = bodyParser
        .parse(state)
        .map(::Block)
}

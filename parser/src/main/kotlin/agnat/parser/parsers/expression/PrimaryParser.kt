package agnat.parser.parsers.expression

import agnat.ast.node.Expression
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.combinators.AnyOf
import agnat.parser.failure.ExpectedExpression
import agnat.parser.parsers.expression.IdentifierParser.ValueIdentifierParser
import agnat.parser.parsers.expression.literal.ListLiteralParser
import agnat.parser.parsers.expression.literal.SimpleLiteralParser
import agnat.parser.recoverWith

internal object PrimaryParser : Parser<Expression> {

    private val bodyParser = AnyOf(
        BlockParser, ValueIdentifierParser, ParenthesisedParser, ListLiteralParser, SimpleLiteralParser
    )

    override fun parse(state: ParseState): ParseResult<Expression> = bodyParser
        .parse(state)
        .recoverWith { ExpectedExpression(state) }
}

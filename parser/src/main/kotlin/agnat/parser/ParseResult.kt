package agnat.parser

import agnat.failure.Failure

sealed interface ParseResult<out T>

data class Success<T>(
    val parsed: T,
    val newParseState: ParseState
) : ParseResult<T>

interface ParseFailure : Failure, ParseResult<Nothing> {

    val lastGoodState: ParseState
}

inline fun <T, U> ParseResult<T>.map(f: (T) -> U): ParseResult<U> = when (this) {
    is Success<T> -> Success(f(parsed), newParseState)
    is ParseFailure -> this
}

@Suppress("UNCHECKED_CAST")  // Since ParseResult is parameterised on T, if T is non-null at runtime it can be narrowed
fun <T> ParseResult<T?>.invert(): ParseResult<T>? = when (this) {
    is Success -> parsed?.let { this as ParseResult<T> }
    is ParseFailure -> this
}

inline fun <T : U, U> ParseResult<T>.recoverWith(f: () -> ParseResult<U>): ParseResult<U> = when (this) {
    is Success<T> -> this
    is ParseFailure -> f()
}

package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class ExpectedNewline(override val lastGoodState: ParseState) : ParseFailure {

    override val reason: String = "Expected a newline but got ${lastGoodState.current()}"
}

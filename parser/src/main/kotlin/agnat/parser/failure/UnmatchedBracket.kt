package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class UnmatchedBracket(override val lastGoodState: ParseState) : ParseFailure {

    override val reason = "Expected a closing bracket but got ${lastGoodState.current()}"
}

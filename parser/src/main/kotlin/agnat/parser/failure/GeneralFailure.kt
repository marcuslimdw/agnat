package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseResult
import agnat.parser.ParseState

data class GeneralFailure(
    override val reason: String = "An unknown parsing error occurred.",
    override val lastGoodState: ParseState
) : ParseFailure {

    companion object {

        fun <T> of(reason: String, lastGoodState: ParseState): ParseResult<T> = GeneralFailure(reason, lastGoodState)
    }
}

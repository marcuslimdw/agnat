package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class ExpectedValueIdentifier(override val lastGoodState: ParseState) : ParseFailure {

    override val reason: String = "Expected a value identifier but got a type identifier ${lastGoodState.current()}"
}

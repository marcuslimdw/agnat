package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class UnmatchedParenthesis(override val lastGoodState: ParseState) : ParseFailure {

    override val reason = "Expected a closing parenthesis but got ${lastGoodState.current()}"
}

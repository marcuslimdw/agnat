package agnat.parser.failure

import agnat.lexer.TokenType
import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class UnexpectedToken(val expected: Set<TokenType>, override val lastGoodState: ParseState) : ParseFailure {

    constructor(expected: TokenType, lastGoodState: ParseState) : this(setOf(expected), lastGoodState)

    override val reason = "Expected a token of type $expected but got ${lastGoodState.current()}"
}

package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class UnmatchedBrace(override val lastGoodState: ParseState) : ParseFailure {

    override val reason = "Expected a closing brace but got ${lastGoodState.current()}"
}

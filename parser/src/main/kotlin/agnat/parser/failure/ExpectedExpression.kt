package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class ExpectedExpression(override val lastGoodState: ParseState) : ParseFailure {

    override val reason: String = "Expected an expression but got ${lastGoodState.current()}"
}

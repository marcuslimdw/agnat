package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class ExpectedTypeIdentifier(override val lastGoodState: ParseState) : ParseFailure {

    override val reason: String = "Expected a type identifier but got a value identifier ${lastGoodState.current()}"
}

package agnat.parser.failure

import agnat.parser.ParseFailure
import agnat.parser.ParseState

data class ExpectedTypeAnnotation(override val lastGoodState: ParseState) : ParseFailure {

    override val reason: String = "Expected a type annotation for identifier ${lastGoodState.current()}"
}

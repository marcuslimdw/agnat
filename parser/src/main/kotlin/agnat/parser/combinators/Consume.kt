package agnat.parser.combinators

import agnat.ast.node.NoOp
import agnat.lexer.TokenType
import agnat.parser.*
import agnat.parser.failure.UnexpectedToken

internal class Consume(
    private val tokenType: TokenType,
    private val failure: (ParseState) -> ParseFailure = { UnexpectedToken(tokenType, it) }
) : Parser<NoOp> {

    override fun parse(state: ParseState): ParseResult<NoOp> {
        val token = state.current()
        return when (token.tokenType) {
            tokenType -> Success(NoOp, state.advance())
            else -> failure(state)
        }
    }
}

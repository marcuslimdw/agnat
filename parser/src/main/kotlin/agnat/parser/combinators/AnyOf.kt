package agnat.parser.combinators

import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.recoverWith

internal class AnyOf<T>(private val parser: Parser<T>, private vararg val parsers: Parser<T>) : Parser<T> {

    override fun parse(state: ParseState): ParseResult<T> = parsers.fold(parser.parse(state)) { acc, next ->
        acc.recoverWith { next.parse(state) }
    }
}

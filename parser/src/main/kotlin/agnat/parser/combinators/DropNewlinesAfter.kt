package agnat.parser.combinators

import agnat.lexer.TokenType.NEWLINE
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser

internal class DropNewlinesAfter<T>(private val parser: Parser<T>) : Parser<T> {

    override fun parse(state: ParseState): ParseResult<T> = Dyad
        .first(parser, Multiple(Consume(NEWLINE)))
        .parse(state)
}

package agnat.parser.combinators

import agnat.parser.*

internal class Optional<T, U : T>(private val parser: Parser<T>, private val fallback: U) : Parser<T> {

    override fun parse(state: ParseState): ParseResult<T> = parser
        .parse(state)
        .recoverWith { Success(fallback, state) }
}

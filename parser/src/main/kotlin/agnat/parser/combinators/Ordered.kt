package agnat.parser.combinators

import agnat.parser.*

internal class Ordered<T>(private vararg val parsers: Parser<T>) : Parser<List<T>> {

    override fun parse(state: ParseState): ParseResult<List<T>> {
        val accumulator = mutableListOf<T>()
        var currentState = state
        for (parser in parsers) {
            when (val result = parser.parse(currentState)) {
                is Success -> {
                    accumulator.add(result.parsed)
                    currentState = result.newParseState
                }
                is ParseFailure -> return result
            }
        }
        return Success(accumulator, currentState)
    }
}

@Suppress("UNCHECKED_CAST")
internal class Dyad<A, B, R>(
    private val parserA: Parser<A>,
    private val parserB: Parser<B>,
    private val f: (A, B) -> R
) : Parser<R> {

    override fun parse(state: ParseState): ParseResult<R> = Ordered(parserA, parserB)
        .parse(state)
        .map { result ->
            val (a, b) = result
            f(a as A, b as B)
        }

    companion object {

        fun <A : R, R> first(parserA: Parser<A>, parserB: Parser<*>): Dyad<A, *, R> = Dyad(
            parserA, parserB
        ) { first, _ -> first }

        fun <B : R, R> second(parserA: Parser<*>, parserB: Parser<B>): Dyad<*, B, R> = Dyad(
            parserA, parserB
        ) { _, second -> second }
    }
}

@Suppress("UNCHECKED_CAST")
internal class Triad<A, B, C, R>(
    private val parserA: Parser<A>,
    private val parserB: Parser<B>,
    private val parserC: Parser<C>,
    private val f: (A, B, C) -> R
) : Parser<R> {

    override fun parse(state: ParseState): ParseResult<R> = Ordered(parserA, parserB, parserC)
        .parse(state)
        .map { result ->
            val (a, b, c) = result
            f(a as A, b as B, c as C)
        }

    companion object {

        fun <B : R, R> second(parserA: Parser<*>, parserB: Parser<B>, parserC: Parser<*>): Triad<*, B, *, R> = Triad(
            parserA, parserB, parserC
        ) { _, second, _ -> second }
    }
}

@Suppress("UNCHECKED_CAST")
internal class Tetrad<A, B, C, D, R>(
    private val parserA: Parser<A>,
    private val parserB: Parser<B>,
    private val parserC: Parser<C>,
    private val parserD: Parser<D>,
    private val f: (A, B, C, D) -> R
) : Parser<R> {

    override fun parse(state: ParseState): ParseResult<R> = Ordered(parserA, parserB, parserC, parserD)
        .parse(state)
        .map { result ->
            val (a, b, c, d) = result
            f(a as A, b as B, c as C, d as D)
        }
}

@Suppress("UNCHECKED_CAST")
internal class Pentad<A, B, C, D, E, R>(
    private val parserA: Parser<A>,
    private val parserB: Parser<B>,
    private val parserC: Parser<C>,
    private val parserD: Parser<D>,
    private val parserE: Parser<E>,
    private val f: (A, B, C, D, E) -> R
) : Parser<R> {

    override fun parse(state: ParseState): ParseResult<R> = Ordered(parserA, parserB, parserC, parserD, parserE)
        .parse(state)
        .map { result ->
            val (a, b, c, d, e) = result
            f(a as A, b as B, c as C, d as D, e as E)
        }
}

@Suppress("UNCHECKED_CAST", "LongParameterList")
internal class Hexad<A, B, C, D, E, F, R>(
    private val parserA: Parser<A>,
    private val parserB: Parser<B>,
    private val parserC: Parser<C>,
    private val parserD: Parser<D>,
    private val parserE: Parser<E>,
    private val parserF: Parser<F>,
    private val f: (A, B, C, D, E, F) -> R
) : Parser<R> {

    override fun parse(state: ParseState): ParseResult<R> =
        Ordered(parserA, parserB, parserC, parserD, parserE, parserF)
            .parse(state)
            .map { result ->
                val (a, b, c, d, e, f) = result
                f(a as A, b as B, c as C, d as D, e as E, f as F)
            }

    companion object {

        @Suppress("MagicNumber", "UnusedPrivateMember")  // False positive
        private operator fun <T> List<T>.component6() = get(5)
    }
}

package agnat.parser.combinators

import agnat.lexer.Token
import agnat.lexer.TokenType
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.Success
import agnat.parser.failure.UnexpectedToken

internal class Capture(private val tokenTypes: Collection<TokenType>) : Parser<Token> {

    constructor(tokenType: TokenType) : this(setOf(tokenType))

    override fun parse(state: ParseState): ParseResult<Token> {
        val token = state.current()
        return when (token.tokenType) {
            in tokenTypes -> Success(token, state.advance())
            else -> UnexpectedToken(tokenTypes.toSet(), state)
        }
    }
}

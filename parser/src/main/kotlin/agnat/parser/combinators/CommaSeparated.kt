package agnat.parser.combinators

import agnat.ast.node.NoOp
import agnat.lexer.TokenType.COMMA
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser

internal class CommaSeparated<T>(
    private val openParser: Parser<*>,
    private val baseElementParser: Parser<T>,
    private val closeParser: Parser<*>
) : Parser<List<T>> {

    // The cast here is safe because the result of Optional<T> is T | NoOp, but we don't have union types.
    @Suppress("UNCHECKED_CAST")
    override fun parse(state: ParseState): ParseResult<List<T>> {
        val elementParser = Dyad.first(DropNewlinesAfter(baseElementParser), DropNewlinesAfter(Consume(COMMA)))
        val initParser = Multiple(elementParser)
        val lastParser = DropNewlinesAfter(Optional(baseElementParser, NoOp))
        return Tetrad(DropNewlinesAfter(openParser), initParser, lastParser, closeParser) { _, elements, last, _ ->
            when (last) {
                is NoOp -> elements.toList()
                else -> elements + last as T
            }
        }.parse(state)
    }
}

package agnat.parser.combinators

import agnat.ast.node.NoOp
import agnat.lexer.TokenType
import agnat.parser.ParseResult
import agnat.parser.ParseState
import agnat.parser.Parser
import agnat.parser.Success
import agnat.parser.failure.UnexpectedToken

internal class Lookahead(private val tokenType: TokenType) : Parser<NoOp> {

    override fun parse(state: ParseState): ParseResult<NoOp> {
        val token = state.current()
        return when (token.tokenType) {
            tokenType -> Success(NoOp, state)
            else -> UnexpectedToken(tokenType, state)
        }
    }
}

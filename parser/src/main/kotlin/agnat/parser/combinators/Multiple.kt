package agnat.parser.combinators

import agnat.parser.*
import agnat.util.Cons
import agnat.util.LinkedList
import agnat.util.Nil

internal class Multiple<T>(private val parser: Parser<T>, private val minimum: Int = 0) : Parser<List<T>> {

    override fun parse(state: ParseState): ParseResult<List<T>> = iterate(Nil, state)

    private tailrec fun iterate(accumulator: LinkedList<T>, state: ParseState): ParseResult<List<T>> =
        when (val next = parser.parse(state)) {
            is Success -> iterate(Cons(next.parsed, accumulator), next.newParseState)
            is ParseFailure -> {
                val parsed = accumulator.reversed().toList()
                if (parsed.size >= minimum) Success(parsed, state) else next
            }
        }
}

package agnat.ast

import agnat.ast.node.Declaration
import agnat.ast.node.NoOp
import agnat.ast.node.Return

interface StatementVisitor<out R, in C> {

    fun visitDeclaration(declaration: Declaration, context: C): R

    fun visitNoOp(noOp: NoOp, context: C): R

    fun visitReturn(returnStatement: Return, context: C): R
}

package agnat.ast

interface NodeVisitor<out R, in C> : ExpressionVisitor<R, C>, StatementVisitor<R, C>

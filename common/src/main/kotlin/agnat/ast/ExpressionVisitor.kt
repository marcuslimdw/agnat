package agnat.ast

import agnat.ast.node.*

interface ExpressionVisitor<out R, in C> {

    fun visitBinaryExpression(binaryExpression: BinaryExpression, context: C): R

    fun visitBlock(block: Block, context: C): R

    fun visitCall(call: Call, context: C): R

    fun visitIdentifier(identifier: Identifier, context: C): R

    fun visitIfExpression(ifExpression: IfExpression, context: C): R

    fun visitLiteral(literal: Literal, context: C): R

    fun visitUnaryExpression(unaryExpression: UnaryExpression, context: C): R
}

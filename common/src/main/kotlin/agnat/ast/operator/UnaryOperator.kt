package agnat.ast.operator

enum class UnaryOperator {
    ARITHMETIC_NEGATE,
    LOGICAL_NEGATE
}

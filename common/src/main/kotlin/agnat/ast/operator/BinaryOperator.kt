package agnat.ast.operator

enum class BinaryOperator {
    LOGICAL_AND,
    LOGICAL_OR,

    COMPARE_EQUAL,
    COMPARE_NOT_EQUAL,
    COMPARE_GREATER,
    COMPARE_GREATER_EQUAL,
    COMPARE_LESS,
    COMPARE_LESS_EQUAL,

    ADD,
    SUBTRACT,

    MULTIPLY,
    DIVIDE,
}

package agnat.ast.node

import agnat.ast.operator.BinaryOperator

data class BinaryExpression(
    val operator: BinaryOperator,
    val left: Expression,
    val right: Expression
) : Expression

package agnat.ast.node

data class IfExpression(
    val condition: Expression,
    val trueBranch: Expression,
    val falseBranch: Expression
) : Expression

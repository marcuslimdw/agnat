package agnat.ast.node

sealed interface Identifier : Expression {

    val name: String
}

@JvmInline
value class TypeIdentifier(override val name: String) : Identifier

sealed interface ValueIdentifier : Identifier

@JvmInline
value class UntypedValueIdentifier(override val name: String) : ValueIdentifier {

    fun withType(type: TypeIdentifier) = TypedValueIdentifier(name, type)
}

data class TypedValueIdentifier(override val name: String, val type: TypeIdentifier) : ValueIdentifier

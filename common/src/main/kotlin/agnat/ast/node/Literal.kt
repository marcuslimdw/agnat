package agnat.ast.node

sealed interface Literal : Expression

@JvmInline
value class BooleanLiteral(val value: Boolean) : Literal

@JvmInline
value class IntLiteral(val value: Long) : Literal

@JvmInline
value class FloatLiteral(val value: Double) : Literal

@JvmInline
value class StringLiteral(val value: String) : Literal

@JvmInline
value class ListLiteral(val value: List<Expression>) : Literal

object UnitLiteral : Literal

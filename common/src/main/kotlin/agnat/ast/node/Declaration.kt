package agnat.ast.node

sealed interface Declaration : Statement {

    val identifier: Identifier
}

data class EnumDeclaration(
    override val identifier: TypeIdentifier,
    val members: List<EnumMemberDeclaration>
) : Declaration {

    data class EnumMemberDeclaration(
        val identifier: TypeIdentifier,
        val members: List<TypedValueIdentifier>
    )
}

data class FunctionDeclaration(
    override val identifier: UntypedValueIdentifier,
    val parameters: List<TypedValueIdentifier>,
    val body: Expression,
    val returnType: TypeIdentifier
) : Declaration

data class ImplementationDeclaration(
    override val identifier: TypeIdentifier,
    val members: List<FunctionDeclaration>
) : Declaration

data class StructDeclaration(
    override val identifier: TypeIdentifier,
    val members: List<TypedValueIdentifier>
) : Declaration

data class VariableDeclaration(
    override val identifier: ValueIdentifier,
    val value: Expression
) : Declaration

package agnat.ast.node

import agnat.ast.operator.UnaryOperator

data class UnaryExpression(
    val operator: UnaryOperator,
    val value: Expression,
) : Expression

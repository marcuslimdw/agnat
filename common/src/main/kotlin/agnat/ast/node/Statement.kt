package agnat.ast.node

import agnat.ast.StatementVisitor

sealed interface Statement : Node {

    fun <R, C> acceptStatementVisitor(visitor: StatementVisitor<R, C>, context: C): R = when (this) {
        is Declaration -> visitor.visitDeclaration(this, context)
        is NoOp -> visitor.visitNoOp(this, context)
        is Return -> visitor.visitReturn(this, context)
    }
}

package agnat.ast.node

data class Return(val expression: Expression) : Statement

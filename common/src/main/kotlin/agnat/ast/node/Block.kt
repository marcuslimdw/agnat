package agnat.ast.node

data class Block(
    val nodes: List<Node>
) : Expression {

    override fun toString(): String = "Block"
}

package agnat.ast.node

data class Call(
    val callee: Expression,
    val arguments: List<Expression>
) : Expression

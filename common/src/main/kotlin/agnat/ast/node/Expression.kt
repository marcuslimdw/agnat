package agnat.ast.node

import agnat.ast.ExpressionVisitor

sealed interface Expression : Node {

    fun <R, C> acceptExpressionVisitor(visitor: ExpressionVisitor<R, C>, context: C): R = when (this) {
        is BinaryExpression -> visitor.visitBinaryExpression(this, context)
        is Block -> visitor.visitBlock(this, context)
        is Call -> visitor.visitCall(this, context)
        is Identifier -> visitor.visitIdentifier(this, context)
        is IfExpression -> visitor.visitIfExpression(this, context)
        is Literal -> visitor.visitLiteral(this, context)
        is UnaryExpression -> visitor.visitUnaryExpression(this, context)
    }
}

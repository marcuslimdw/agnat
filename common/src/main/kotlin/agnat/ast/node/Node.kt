package agnat.ast.node

import agnat.ast.NodeVisitor

sealed interface Node {

    fun <R, C> accept(visitor: NodeVisitor<R, C>, context: C): R = when (this) {
        is Expression -> acceptExpressionVisitor(visitor, context)
        is Statement -> acceptStatementVisitor(visitor, context)
    }
}

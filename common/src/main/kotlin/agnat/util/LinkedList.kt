package agnat.util

sealed interface LinkedList<out T> : Collection<T> {

    fun toList(): List<T>

    fun filter(f: (T) -> Boolean): LinkedList<T> {
        tailrec fun filterRecursive(acc: LinkedList<T>, rest: LinkedList<T>): LinkedList<T> =
            when (rest) {
                is Cons -> {
                    val (head, tail) = rest
                    val newAcc = if (f(head)) Cons(head, acc) else acc
                    filterRecursive(newAcc, tail)
                }
                is Nil -> acc
            }

        return filterRecursive(Nil, this).reversed()
    }

    fun <U> fold(initial: U, f: (U, T) -> U): U {
        tailrec fun foldRecursive(cons: Cons<T>, initial: U, f: (U, T) -> U): U {
            val (head, tail) = cons
            val next = f(initial, head)
            return when (tail) {
                is Cons<T> -> foldRecursive(tail, next, f)
                is Nil -> next
            }
        }

        return when (this) {
            is Cons -> foldRecursive(this, initial, f)
            is Nil -> initial
        }
    }

    fun reversed(): LinkedList<T> = fold<LinkedList<T>>(Nil) { acc, next -> Cons(next, acc) }

    companion object {

        fun <T> from(vararg values: T): LinkedList<T> = values
            .fold(Nil.of()) { acc, next -> Cons(next, acc) }
    }
}

data class Cons<T>(val head: T, val tail: LinkedList<T>) : LinkedList<T> {

    override val size: Int = tail.size + 1

    override fun isEmpty(): Boolean = false

    override fun toList(): List<T> {
        val result = mutableListOf(head)
        var next = tail
        while (next is Cons) {
            result.add(next.head)
            next = next.tail
        }
        return result
    }

    override fun toString(): String {
        val builder = StringBuilder("[$head")
        var next = tail
        while (next is Cons) {
            builder.append(", ", next.head.toString())
            next = next.tail
        }
        builder.append("]")
        return builder.toString()
    }

    override fun contains(element: T): Boolean {
        var (current, next) = this
        while (next is Cons) {
            if (current == element) return true
            current = next.head
            next = next.tail
        }
        return false
    }

    override fun containsAll(elements: Collection<T>): Boolean {
        val toCheck = elements.toMutableSet()
        var (current, next) = this
        while (next is Cons) {
            if (current in elements) {
                toCheck.remove(current)
                if (toCheck.isEmpty()) return true
            }
            current = next.head
            next = next.tail
        }
        return false
    }

    override fun iterator(): Iterator<T> = object : Iterator<T> {

        private var current: LinkedList<T> = this@Cons

        override fun hasNext(): Boolean = current is Cons<*>

        override fun next(): T = if (current is Cons<*>) {
            val (head, tail) = current as Cons
            current = tail
            head
        } else throw NoSuchElementException("Cons iterator is exhausted.")
    }
}

object Nil : LinkedList<Nothing> {

    override val size: Int = 0

    override fun isEmpty(): Boolean = true

    override fun contains(element: Nothing): Boolean = false

    override fun containsAll(elements: Collection<Nothing>): Boolean = false

    override fun iterator(): Iterator<Nothing> = object : Iterator<Nothing> {
        override fun hasNext(): Boolean = false

        override fun next(): Nothing = throw NoSuchElementException("No elements in Nil.")
    }

    override fun toList(): List<Nothing> = emptyList()

    override fun toString(): String = "[]"

    fun <T> of(): LinkedList<T> = this
}

fun <T, U> Collection<T>.mapToLinkedList(f: (T) -> U): LinkedList<U> = when (isEmpty()) {
    true -> Nil
    false -> fold<T, LinkedList<U>>(Nil) { acc, next -> Cons(f(next), acc) }.reversed()
}

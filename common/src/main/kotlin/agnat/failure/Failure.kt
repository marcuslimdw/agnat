package agnat.failure

interface Failure {

    val reason: String
}

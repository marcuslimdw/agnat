package agnat.util

import assertk.assertThat
import assertk.assertions.isEmpty
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class LinkedListKtTest {

    @Nested
    inner class FilterTest {

        @Test
        fun `should leave Nil unchanged`() {
            assertThat(Nil.filter { throw IllegalArgumentException() }).isEqualTo(Nil)
        }

        @Test
        fun `should filter out values that fail a predicate`() {
            val actual = Cons(1, Cons(2, Cons(3, Nil))).filter { it % 2 == 0 }
            assertThat(actual).isEqualTo(Cons(2, Nil))
        }
    }

    @Nested
    inner class ReversedTest {

        @Test
        fun `should leave Nil unchanged`() {
            assertThat(Nil.reversed()).isEqualTo(Nil)
        }

        @Test
        fun `should reverse a non-Nil LinkedList`() {
            val actual = Cons(1, Cons(2, Cons(3, Nil))).reversed()
            val expected = Cons(3, Cons(2, Cons(1, Nil)))
            assertThat(actual).isEqualTo(expected)
        }
    }

    @Nested
    inner class ToListTest {

        @Test
        fun `should convert Nil to an empty List`() {
            assertThat(Nil.toList()).isEmpty()
        }

        @Test
        fun `should convert a non-Nil LinkedList to a List`() {
            val actual = Cons(1, Cons(2, Cons(3, Nil))).toList()
            val expected = listOf(1, 2, 3)
            assertThat(actual).isEqualTo(expected)
        }

    }

    @Nested
    inner class MapToLinkedListTest {

        @Test
        fun `should convert an empty collection to Nil`() {
            val actual = emptyList<Int>().mapToLinkedList { throw IllegalStateException() }
            assertThat(actual).isEqualTo(Nil)
        }

        @Test
        fun `should convert a nonempty collection, applying a function over its elements, to a non-Nil LinkedList`() {
            val actual = listOf(1, 2, 3).mapToLinkedList { it * 2 }
            val expected = Cons(2, Cons(4, Cons(6, Nil)))
            assertThat(actual).isEqualTo(expected)
        }
    }
}
